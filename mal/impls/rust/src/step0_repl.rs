use rustyline::error::ReadlineError;
use rustyline::{Editor, Result};

fn read(src: &str) -> String {
    src.to_string()
}

fn eval(ast: &str) -> String {
    ast.to_string()
}

fn print(text: &str) {
    println!("{}", text);
}

fn rep(input: String) {
    let ast = read(&input);
    let result = eval(&ast);
    print(&result);
}

fn repl() -> Result<()> {
    let mut rl = Editor::<()>::new()?;
    loop {
        let readline = rl.readline("user> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                rep(line)
            }
            Err(ReadlineError::Interrupted) => {
                break;
            }
            Err(ReadlineError::Eof) => {
                break;
            }
            Err(err) => {
                eprintln!("Error: {:?}", err);
                break;
            }
        }
    }

    Ok(())
}

fn main() -> Result<()> {
    repl()
}
