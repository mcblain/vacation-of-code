import { stdin, stdout, exit } from 'node:process';
import * as readline from 'node:readline/promises';

const rl = readline.createInterface({ input: stdin, output: stdout });

function read(src) {
  return src;
}

function exec(ast) {
  return ast;
}

function print(result) {
  stdout.write(result + "\n")
}

function rep(input) {
  const ast = read(input);
  const result = exec(ast);
  print(result);
}

async function repl() {
  for (; ;) {
    const line = await rl.question('user> ');
    if (line) {
      rep(line);
    }
  }
}

async function main() {
  await repl();
  rl.close();
  exit(0);
}

main()
