#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <readline/history.h>
#include <readline/readline.h>

static char* PROMPT = "user> ";

char* mal_read(char* source) {
    return source;
}

char* mal_eval(char* source) {
    return source;
}

void mal_print(char* str) {
    printf("%s\n", str);
    return;
}

void mal_rep(char* src) {
    char* x = mal_read(src);
    char* result = mal_eval(x);
    mal_print(result);
}

void mal_repl(void) {
    char* line;

    while ((line = readline(PROMPT)) != NULL) {
        if (0 < strlen(line)) {
            add_history(line);
            mal_rep(line);
        }
        free(line);
    }
}

int main(int argc, char** argv) {
    mal_repl();

    return 0;
}
