package main

import "core:c/libc"
import "core:fmt"
import gl "vendor:OpenGL"
import "vendor:glfw"

WINDOW_WIDTH :: 800
WINDOW_HEIGHT :: 600

MAJOR_VERSION :: 3
MINOR_VERSION :: 3

PROGRAMNAME :: "Hello, Window"

framebuffer_size_callback :: proc "c" (window: glfw.WindowHandle, width: i32, height: i32) {
	gl.Viewport(0, 0, width, height)
}

process_input :: proc(window: glfw.WindowHandle) {
	using glfw

	if GetKey(window, KEY_ESCAPE) == PRESS {
		SetWindowShouldClose(window, true)
	}
}

main :: proc() {
	if glfw.Init() != 1 {
		fmt.eprintln("Failed initializing glfw")
		return
	}

	glfw.WindowHint(glfw.RESIZABLE, 1)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR, MAJOR_VERSION)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR, MINOR_VERSION)
	glfw.WindowHint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
	
	window := glfw.CreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, PROGRAMNAME, nil, nil)
	
	if window == nil {
		fmt.eprintln("Failed creating window")
		glfw.Terminate()
		return
	}
	
	glfw.MakeContextCurrent(window)	
	gl.load_up_to(MAJOR_VERSION, MINOR_VERSION, glfw.gl_set_proc_address)
	glfw.SetFramebufferSizeCallback(window, framebuffer_size_callback)

	
	for !glfw.WindowShouldClose(window) {
		// input
		glfw.PollEvents()

		process_input(window)

		// rendering
		gl.ClearColor(0.2, 0.3, 0.3, 1.0);
		gl.Clear(gl.COLOR_BUFFER_BIT); // clean the color buffer

		glfw.SwapBuffers(window)
	}

	glfw.Terminate()
}

