package main

import "core:fmt"
import "vendor:sdl2"
import "vendor:OpenGL"

WINDOW_WIDTH :: 800
WINDOW_HEIGHT :: 600

init_gl :: proc() -> bool {
	using OpenGL
	
	error := GL_Enum.NO_ERROR
	
	return true
}

init :: proc() -> bool {
	using sdl2

	if Init({.VIDEO}) < 0 {
		fmt.println("SDL could not initialize! SDL Error:", GetError())
		return false
	}
	
	GL_SetAttribute(.CONTEXT_MAJOR_VERSION, 3)
	GL_SetAttribute(.CONTEXT_MAJOR_VERSION, 3)
	
	window := CreateWindow(
		"Creating a window",
		WINDOWPOS_UNDEFINED, WINDOWPOS_UNDEFINED,
		WINDOW_WIDTH, WINDOW_HEIGHT,
		{.OPENGL, .SHOWN, .RESIZABLE})

	if window == nil {
		fmt.println(
			"Window could not be created! SDL Error:",
			GetError())
		return false
	}
	
	gl_context := GL_CreateContext(window)
	
	if gl_context == nil {
		fmt.println("OpenGL context could not be created! SDL Error:", GetError())
		return false
	}
	
	if GL_SetSwapInterval(1) < 0 {
		fmt.println("Warning: Unable to set VSync! SDL Error:", GetError())
	}
	
	if !init_gl() {
		fmt.println("Unable to initialize OpenGL")
		return false
	}

	return true
} 

main :: proc() {
	init()
	fmt.println("Finished")
}

