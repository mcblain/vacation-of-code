package main

import "core:c/libc"
import "core:fmt"
import gl "vendor:OpenGL"
import "vendor:glfw"

WINDOW_WIDTH :: 800
WINDOW_HEIGHT :: 600

MAJOR_VERSION :: 3
MINOR_VERSION :: 3

PROGRAMNAME :: "Hello, Triangle"

framebuffer_size_callback :: proc "c" (window: glfw.WindowHandle, width: i32, height: i32) {
	gl.Viewport(0, 0, width, height)
}

check_and_print_compilation_error :: proc(shader_handle: u32) {
	compile_success : i32 = 1
	info_log : []u8 = make([]u8, 512)
	
	gl.GetShaderiv(shader_handle, gl.COMPILE_STATUS, &compile_success)
	
	if compile_success == 0 {
		gl.GetShaderInfoLog(shader_handle, 512, nil, ([^]u8)(&info_log[0]))
		fmt.println("ERROR::SHADER::COMPILATION_FAILED", transmute(string)info_log)
	}	
}

check_and_print_linking_error :: proc(program_handle: u32) {
	linking_success : i32 = 1
	info_log : []u8 = make([]u8, 512)
	
	gl.GetProgramiv(program_handle, gl.COMPILE_STATUS, &linking_success)
	
	if linking_success == 0 {
		gl.GetProgramInfoLog(program_handle, 512, nil, ([^]u8)(&info_log[0]))
		fmt.println("ERROR::SHADER_PROGRAM::LINKING_FAILED", transmute(string)info_log)
	}	
}

process_input :: proc(window: glfw.WindowHandle) {
	using glfw

	if GetKey(window, KEY_ESCAPE) == PRESS {
		SetWindowShouldClose(window, true)
	}
}

main :: proc() {
	if glfw.Init() != 1 {
		fmt.eprintln("Failed initializing glfw")
		return
	}

	glfw.WindowHint(glfw.RESIZABLE, 1)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR, MAJOR_VERSION)
	glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR, MINOR_VERSION)
	glfw.WindowHint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)
	
	window := glfw.CreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, PROGRAMNAME, nil, nil)
	if window == nil {
		fmt.eprintln("Failed creating window")
		glfw.Terminate()
		return
	}
	
	glfw.MakeContextCurrent(window)	
	gl.load_up_to(MAJOR_VERSION, MINOR_VERSION, glfw.gl_set_proc_address)
	gl.Viewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT)	
	glfw.SetFramebufferSizeCallback(window, framebuffer_size_callback)
	
	vertices := [?]f32{
		0.5,  0.5, 0.0,  // top right
		0.5, -0.5, 0.0,  // bottom right
		-0.5, -0.5, 0.0,  // bottom left
		-0.5,  0.5, 0.0 } // top left  
	
	indices := [?]u32 {
		0, 1, 3,
		1, 2, 3,
	}
		
	vertex_shader_source := cstring(#load("vertex.glsl"))
	vertex_shader := gl.CreateShader(gl.VERTEX_SHADER)
	gl.ShaderSource(vertex_shader, 1, &vertex_shader_source, nil)
	gl.CompileShader(vertex_shader)
	check_and_print_compilation_error(vertex_shader)

	fragment_shader_source := cstring(#load("fragment.glsl"))
	fragment_shader := gl.CreateShader(gl.FRAGMENT_SHADER)
	gl.ShaderSource(fragment_shader, 1, &fragment_shader_source, nil)
	gl.CompileShader(fragment_shader)
	check_and_print_compilation_error(fragment_shader)
	
	shader_program := gl.CreateProgram()
	gl.AttachShader(shader_program, vertex_shader)
	gl.AttachShader(shader_program, fragment_shader)
	gl.LinkProgram(shader_program)
	gl.DeleteShader(vertex_shader)
	gl.DeleteShader(fragment_shader)
	check_and_print_linking_error(shader_program)
	
	VAO : u32
	gl.GenVertexArrays(1, &VAO)
	gl.BindVertexArray(VAO)

	VBO : u32;
	gl.GenBuffers(1, &VBO)
	gl.BindBuffer(gl.ARRAY_BUFFER, VBO)
	gl.BufferData(gl.ARRAY_BUFFER, size_of(vertices), &vertices, gl.STATIC_DRAW)	

	EBO : u32
	gl.GenBuffers(1, &EBO)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, EBO)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, size_of(indices), &indices, gl.STATIC_DRAW)	

	gl.VertexAttribPointer(0, 3, gl.FLOAT, gl.FALSE, 3 * size_of(f32), 0)
	gl.EnableVertexAttribArray(0)

	gl.UseProgram(shader_program)

	for !glfw.WindowShouldClose(window) {
		// input
		glfw.PollEvents()

		process_input(window)

		// rendering
		gl.ClearColor(0.2, 0.3, 0.3, 1.0);
		gl.Clear(gl.COLOR_BUFFER_BIT); // clean the color buffer
		
		gl.BindVertexArray(VAO)
		gl.DrawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, nil)
		gl.BindVertexArray(0)

		glfw.SwapBuffers(window)
	}

	glfw.Terminate()
}

