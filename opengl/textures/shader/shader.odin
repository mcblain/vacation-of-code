package shader

import "core:os"
import "core:fmt"
import gl "vendor:OpenGL"
import "core:strings"

Shader :: u32

check_and_print_compilation_error :: proc(shader_handle: u32, shader_name: string) {
	compile_success : i32 = 1
	info_log : []u8 = make([]u8, 512)
	
	gl.GetShaderiv(shader_handle, gl.COMPILE_STATUS, &compile_success)
	
	if compile_success == 0 {
		gl.GetShaderInfoLog(shader_handle, 512, nil, ([^]u8)(&info_log[0]))
		fmt.printf("ERROR::%s::COMPILATION_FAILED: %s", shader_name, transmute(string)info_log)
	}	
}

check_and_print_linking_error :: proc(program_handle: u32) {
	linking_success : i32 = 1
	info_log : []u8 = make([]u8, 512)
	
	gl.GetProgramiv(program_handle, gl.COMPILE_STATUS, &linking_success)
	
	if linking_success == 0 {
		gl.GetProgramInfoLog(program_handle, 512, nil, ([^]u8)(&info_log[0]))
		fmt.println("ERROR::SHADER_PROGRAM::LINKING_FAILED",
					transmute(string)info_log)
	}	
}

create :: proc(vertex_path, fragment_path: string) -> (Shader, bool) {
	vertex_shader, vertex_ok : =
		os.read_entire_file_from_filename(vertex_path)
	if !vertex_ok {
		fmt.eprintln(
			"ERROR::SHADER::VERTEX::FILE_NOT_SUCCESSFULLY_READ")
		return 0, false
	}
	// defer delete(vertex_shader)
	
	fragment_shader, fragment_ok :=
		os.read_entire_file_from_filename(fragment_path)
	if !fragment_ok {
		fmt.eprintln("ERROR::SHADER::FRAGMENT::FILE_NOT_SUCCESSFULLY_READ")
		return 0, false
	}

	// defer delete(fragment_shader)
		
	c_str := (cstring)(&vertex_shader[0])
	vertex := gl.CreateShader(gl.VERTEX_SHADER)
	gl.ShaderSource(vertex, 1, &c_str, nil)
	gl.CompileShader(vertex)
	check_and_print_compilation_error(vertex, "VERTEX")
	
	c_str = (cstring)(&fragment_shader[0])
	fragment := gl.CreateShader(gl.FRAGMENT_SHADER)
	gl.ShaderSource(fragment, 1, &c_str, nil)
	gl.CompileShader(fragment)
	check_and_print_compilation_error(fragment, "FRAGMENT")
	
	id := gl.CreateProgram()
	gl.AttachShader(id, vertex)
	gl.AttachShader(id, fragment)
	gl.LinkProgram(id)
	check_and_print_linking_error(id)
	
	gl.DeleteShader(vertex)
	gl.DeleteShader(fragment)
	
	return id, true
}

use :: proc(shader: Shader) {
	gl.UseProgram(shader)
}

// uniform setters
set_bool :: proc(shader: Shader, name: string, value: bool) {
	c_str := strings.clone_to_cstring(name)
	gl.Uniform1i(gl.GetUniformLocation(shader, cstring(c_str)), i32(value))
	delete(c_str)
}
set_int :: proc(shader: Shader, name: string, value: i32) {
	c_str := strings.clone_to_cstring(name)
	gl.Uniform1i(gl.GetUniformLocation(shader, cstring(c_str)), value)
	delete(c_str)
}
set_f32 :: proc(shader: Shader, name: string, value: f32) {
	c_str := strings.clone_to_cstring(name)
	gl.Uniform1f(gl.GetUniformLocation(shader, cstring(c_str)), value)
	delete(c_str)
}