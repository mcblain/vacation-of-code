export type Token =
  { literal: string, type: TokenType }

export enum TokenType {
  CARET,
  DIVIDE,
  EOF,
  ERROR,
  LPAREN,
  MINUS,
  MULTIPLY,
  NUMBER,
  PERCENT,
  PLUS,
  RPAREN,
}

export function new_token(literal: string, type: TokenType): Token {
  return ({ literal, type })
}

export function stringify_token(t: Token): string {
  const tt = t.type;
  switch (tt) {
    case TokenType.ERROR: return "ERROR: " + t.literal;
    case TokenType.EOF: return "EOF";
    case TokenType.PLUS: return "+";
    case TokenType.MINUS: return "-";
    case TokenType.MULTIPLY: return "*";
    case TokenType.DIVIDE: return "/";
    case TokenType.LPAREN: return "(";
    case TokenType.RPAREN: return ")";
    case TokenType.PERCENT: return "%";
    case TokenType.CARET: return "^";
    case TokenType.NUMBER: return t.literal;
  }
}


