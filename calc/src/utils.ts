export function println(string: string) {
  console.log(string);
}

export function ascii_code(c: string): number {
  if (c.length === 0) {
    return 0;
  }
  return c.charCodeAt(0);
}
