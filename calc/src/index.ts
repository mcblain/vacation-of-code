import {new_lexer} from "./lexer"
import { expr } from "./parser";

function repl() {
  const input = prompt("> ")
  const lexer = new_lexer(input);
  const ast = expr(lexer);
  console.log(ast.to_string());
  console.log("RESULT:", ast.calc())
  return repl()
}

function main() {
  repl()
}

main()
