import { new_token, Token, TokenType } from "./token";
import { ascii_code } from "./utils";

export type Lexer = { source: string, start: number, curr: number, peeked: Token | null }


function is_digit(c: string): boolean {
  if (c.length === 0) {
    return false;
  }
  const code = ascii_code(c);
  return ascii_code("0") <= code && code <= ascii_code("9")
}


export function new_lexer(source: string): Lexer {
  return ({ source, start: 0, curr: 0, peeked: null })
}

function is_at_end(lex: Lexer) {
  return lex.source.length <= lex.curr;
}

function peek(lex: Lexer) {
  return lex.source[lex.curr] || "\0";  
}

function peek_next(lex: Lexer) {  
  return lex.source[lex.curr + 1] || "\0";
}

function advance(lex: Lexer) {
  return lex.source[lex.curr++];
}

function make_token(lex: Lexer, type: TokenType) {
  const token = new_token(lex.source.substring(lex.start, lex.curr), type);
  lex.start = lex.curr;
  return token;
}

function make_error(lex: Lexer, msg: string) {
  const token = new_token(msg, TokenType.ERROR);
  lex.start = lex.curr;
  return token;
}

function skip(lex: Lexer) {
  lex.start += 1;
  lex.curr += 1;
}

function skip_whitespace(lex: Lexer) {
  switch (peek(lex)) {
    case " ":
    case "\t":
    case "\n":
      skip(lex);
  }
}

function number(lex: Lexer) {
  while (is_digit(peek(lex))) {
    advance(lex);
  }

  if (peek(lex) === "." && is_digit(peek_next(lex))) {
    advance(lex);
    while (is_digit(peek(lex))) {
      advance(lex);
    }
  }

  return make_token(lex, TokenType.NUMBER);
}

export function peek_token(lex: Lexer): Token {
  const token = next_token(lex);
  lex.peeked = token;
  return token;
}

export function next_token(lex: Lexer): Token {
  if (lex.peeked !== null) {
    const peeked = lex.peeked;
    lex.peeked = null;
    return peeked;
  }
  
  if (is_at_end(lex)) {
    return make_token(lex, TokenType.EOF);
  }

  skip_whitespace(lex);

  const c = advance(lex);

  if (is_digit(c)) {
    return number(lex);
  }

  switch (c) {
    case '+': return make_token(lex, TokenType.PLUS);
    case '-': return make_token(lex, TokenType.MINUS);
    case '*': return make_token(lex, TokenType.MULTIPLY);
    case '/': return make_token(lex, TokenType.DIVIDE);
    case '^': return make_token(lex, TokenType.CARET);
    case '%': return make_token(lex, TokenType.PERCENT);
    case '(': return make_token(lex, TokenType.LPAREN);
    case ')': return make_token(lex, TokenType.RPAREN);
  }

  return make_error(lex, `Undefined symbol '${c}'`);
}