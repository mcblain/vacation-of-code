import { Expr, GroupingExpr, InfixExpr, NumExpr, PrefixExpr } from "./expr";
import { Lexer, next_token, peek_token } from "./lexer"
import { Token, TokenType } from "./token";

type PrefixParselet = (lexer: Lexer, op: Token) => Expr;
type InfixParselet = (lexer: Lexer, lhs: Expr, op: TokenType) => Expr;

type ExprTable = {
  prefix: (null | PrefixParselet)[],
  infix: (null | InfixParselet)[],
  bp: number[]
};

function consume(lex: Lexer, expected: TokenType, msg: string) {
  if (peek_token(lex).type === expected) {
    next_token(lex);
    return;
  }

  console.error(msg);
  return;
}

function unary(lex: Lexer, op: Token): Expr {
  const rhs = expr(lex);
  return new PrefixExpr(op.type, rhs);
}

function grouping(lex: Lexer): Expr {
  const rhs = expr(lex);
  consume(lex, TokenType.RPAREN, "Expected ')'");
  return new GroupingExpr(rhs);
}

function num_expr(_: Lexer, tok: Token): Expr {
  const num_str = tok.literal;
  const num = parseInt(num_str);
  if (Number.isNaN(num)) {
    return new NumExpr(0);
  }

  return new NumExpr(num);
}

function binary(lex: Lexer, lhs: Expr, op: TokenType): Expr {
  const bp = get_bp(op);
  const rhs = expr_bp(lex, bp + 1);
  return new InfixExpr(op, lhs, rhs);
}

const EXPR_TABLE: ExprTable = {
  // CARET,    DIVIDE, EOF,  ERROR, LPAREN,   MINUS,  MULTIPLY, NUMBER,   PERCENT, PLUS,   RPAREN
  prefix: [null, null, null, null, grouping, unary, null, num_expr, null, unary, null],
  infix: [binary, binary, null, null, null, binary, binary, null, binary, binary, null],
  bp: [3, 2, 0, 0, 0, 1, 2, 0, 2, 1, 0],
}

function get_prefix(type: TokenType): PrefixParselet | null {
  return EXPR_TABLE.prefix[type];
}
function get_infix(type: TokenType): InfixParselet | null {
  return EXPR_TABLE.infix[type];
}
function get_bp(type: TokenType): number {
  return EXPR_TABLE.bp[type];
}

export function expr(lex: Lexer): Expr {
  return expr_bp(lex, 0);
}

function expr_bp(lex: Lexer, bp: number): Expr {
  const tok = next_token(lex);
  const prefix = get_prefix(tok.type);
  if (!prefix) {
    console.error("Error, invalid prefix token:", tok);
    return;
  }

  let lhs = prefix(lex, tok);

  for (; ;) {
    const op = peek_token(lex);
    if (get_bp(op.type) < bp) {
      break;
    }

    const infix = get_infix(op.type);
    if (!infix) {
      break;
    }

    next_token(lex);
    lhs = infix(lex, lhs, op.type);
  }


  return lhs;
}

