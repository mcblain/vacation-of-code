import { TokenType } from "./token";

export interface Expr {
  to_string: () => string;
  calc: () => number;
};

export class NumExpr implements Expr {
  val: number;

  constructor(val: number) {
    this.val = val;
  };

  to_string = () => this.val.toString();
  calc = () => this.val;
}

type PrefixTableEntry = { char: string, fn: (x: number) => number };
type PrefixTable = { [key: number]: PrefixTableEntry };
export class PrefixExpr implements Expr {
  type: TokenType;
  rhs: Expr;

  table: PrefixTable = {
    [TokenType.MINUS]: { char: "-", fn: (x: number) => -x },
    [TokenType.PLUS]: { char: "+", fn: (x: number) => x },
  };


  constructor(type: TokenType, rhs: Expr) {
    this.type = type;
    this.rhs = rhs;
  }

  expr =
    (): PrefixTableEntry =>
      this.table[this.type];

  to_string = () => {
    const rhs = this.rhs.to_string();
    const char = this.expr().char;
    return `(${char} ${rhs})`;
  }


  calc = () => {
    const x = this.rhs.calc();
    return this.expr().fn(x);
  }
}

type InfixTableEntry = { char: string, fn: (x: number, y: number) => number };
type InfixTable = { [key: number]: InfixTableEntry };
export class InfixExpr implements Expr {
  type: TokenType;
  lhs: Expr;
  rhs: Expr;

  table: InfixTable = {
    [TokenType.PLUS]: { char: "+", fn: (x: number, y: number) => x + y },
    [TokenType.MINUS]: { char: "-", fn: (x: number, y: number) => x - y },
    [TokenType.MULTIPLY]: { char: "*", fn: (x: number, y: number) => x * y },
    [TokenType.DIVIDE]: { char: "/", fn: (x: number, y: number) => x / y },
    [TokenType.CARET]: { char: "^", fn: (x: number, y: number) => Math.pow(x, y) },
    [TokenType.PERCENT]: { char: "%", fn: (x: number, y: number) => x % y },
  };

  constructor(type: TokenType, lhs: Expr, rhs: Expr) {
    this.type = type;
    this.lhs = lhs;
    this.rhs = rhs;
  }

  expr =
    (): InfixTableEntry =>
      this.table[this.type];

  to_string = () => {
    const char = this.expr().char;
    const lhs = this.lhs.to_string();
    const rhs = this.rhs.to_string();
    return `(${char} ${lhs} ${rhs})`;
  }


  calc = () => {
    const x = this.lhs.calc();
    const y = this.rhs.calc();
    return this.expr().fn(x, y);
  }
}

export class GroupingExpr implements Expr {
  expr: Expr;

  constructor(expr: Expr) {
    this.expr = expr;
  }

  to_string = () => {
    const str_expr = this.expr.to_string();
    return `(${str_expr})`;
  }


  calc = () => {
    return this.expr.calc();
  }
}