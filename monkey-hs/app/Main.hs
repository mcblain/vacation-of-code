module Main where

import System.IO
import Lexer

repl :: IO ()
repl = do
  putStr ">> "
  hFlush stdout
  line <- getLine
  print $ tokenizeStr line
  repl
  -- return ()

main :: IO ()
main = repl
