module Lexer (tokenizeStr) where

import Data.Char
import Token qualified as T

data Lexer = Lexer
  { tokens :: [T.Token],
    literal :: String,
    input :: String
  }

isWhiteSpace :: Char -> Bool
isWhiteSpace c = c `elem` [' ', '\n', '\t', '\r']

isIdent :: Char -> Bool
isIdent c = isAlpha c || c == '_'

skipWhiteSpaces :: Lexer -> Lexer
skipWhiteSpaces lexer@(Lexer _ _ []) = tokenize lexer
skipWhiteSpaces lexer@(Lexer tokens literal (c : cs))
  | isWhiteSpace c = skipWhiteSpaces (Lexer tokens literal cs)
  | otherwise = lexer

matchKeyword :: T.Literal -> T.Token
matchKeyword literal = case literal of
  "let" -> make T.Let
  "fn" -> make T.Function
  "true" -> make T.True
  "false" -> make T.False
  "if" -> make T.If
  "else" -> make T.Else
  "return" -> make T.Return
  _ -> make T.Ident
  where
    make kind = T.Token kind literal

readWhile :: (Char -> Bool) -> (T.Literal -> T.Token) -> Lexer -> Lexer
readWhile cond constr lexer@(Lexer tokens literal "")
  | literal == "" = tokenize lexer
  | otherwise = tokenize (Lexer (constr (reverse literal) : tokens) "" "")
readWhile cond constr lexer@(Lexer tokens literal (c : cs))
  | cond c = readWhile cond constr (Lexer tokens (c : literal) cs)
  | literal == "" = lexer
  | otherwise = tokenize (Lexer (constr (reverse literal) : tokens) "" (c : cs))

readIdentifier :: Lexer -> Lexer
readIdentifier = readWhile isIdent matchKeyword

readNumber :: Lexer -> Lexer
readNumber = readWhile isDigit (T.Token T.Int)

readSimpleToken :: Lexer -> Lexer
readSimpleToken lexer@(Lexer tokens literal (c : src)) =
  case c of
    '+' -> consume T.Plus
    '-' -> consume T.Minus
    '/' -> consume T.Slash
    '*' -> consume T.Asterisk
    '<' -> consume T.Lt
    '>' -> consume T.Gt
    ';' -> consume T.Semicolon
    '(' -> consume T.LParen
    ')' -> consume T.RParen
    ',' -> consume T.Comma
    '{' -> consume T.RBrace
    '}' -> consume T.RBrace
    '=' -> if next == '=' then consume2 T.Eq else consume T.Assign
    '!' -> if next == '=' then consume2 T.NotEq else consume T.Bang
    _ -> consume T.Illegal
  where
    next = head src
    consume kind = Lexer (T.Token kind [c] : tokens) "" src
    consume2 kind = Lexer (T.Token kind [c, next] : tokens) "" (tail src)
readSimpleToken lexer = lexer

tokenize :: Lexer -> Lexer
tokenize lexer@(Lexer _ _ "") = lexer
tokenize lexer =
  tokenize $
    readSimpleToken $
      readNumber $
        readIdentifier $
          skipWhiteSpaces lexer

tokenizeStr :: String -> [T.Token]
tokenizeStr input = reverse (tokens (tokenize (Lexer [] "" input)))

test =
  tokenizeStr
    "        let five = 5;\n\
    \let ten = 10;\n\
    \\n\
    \let add = fn(x, y) {\n\
    \  x + y;\n\
    \};\n\
    \\n\
    \let result = add(five, ten);\n\
    \!-/*5;\n\
    \5 < 10 > 5;\n\
    \\n\
    \if (5 < 10) {\n\
    \  return true;\n\
    \} else {\n\
    \  return false;\n\
    \}\n\
    \\n\
    \10 == 10;\n\
    \10 != 9;\n\
    \\n\
    \"