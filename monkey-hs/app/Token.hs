module Token
  ( TokenKind (..),
    Token (..),
    Literal,
    makeToken,
  )
where

data TokenKind
  = Illegal
  | Eof
  | Ident
  | Int
  | Assign
  | Plus
  | Minus
  | Bang
  | Asterisk
  | Slash
  | Eq
  | NotEq
  | Lt
  | Gt
  | Comma
  | Semicolon
  | LParen
  | RParen
  | LBrace
  | RBrace
  | Function
  | Let
  | True
  | False
  | If
  | Else
  | Return
  deriving (Show)

type Literal = String

data Token = Token TokenKind Literal deriving (Show)

makeToken :: TokenKind -> Literal -> Token
makeToken = Token
