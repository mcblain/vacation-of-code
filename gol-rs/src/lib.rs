use dashmap::DashSet;
use rayon::prelude::*;
use std::mem;

pub type Coord = i32;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Cell {
    pub x: Coord,
    pub y: Coord,
}

impl Cell {
    #[inline(always)]
    pub fn new(x: Coord, y: Coord) -> Self {
        Self { x, y }
    }
}

pub type Cells = DashSet<Cell>;

pub struct GameOfLife {
    pub cells: Cells,
    prev_cells: Cells,
}

impl Default for GameOfLife {
    fn default() -> Self {
        Self::new()
    }
}

impl GameOfLife {
    pub fn new() -> Self {
        GameOfLife {
            prev_cells: Cells::new(),
            cells: Cells::new(),
        }
    }

    #[inline(always)]
    pub fn set_cell(&mut self, cell: Cell) {
        self.cells.insert(cell);
    }

    #[inline(always)]
    pub fn remove_cell(&mut self, cell: &Cell) {
        self.cells.remove(cell);
    }

    fn check_cell(&self, cell: &Cell) -> bool {
        let mut neighbours: u8 = 0;
        let cell_alive = self.check_alive(cell);

        let x = cell.x;
        let y = cell.y;

        neighbours += self.check_alive(&Cell::new(x - 1, y - 1)) as u8;
        neighbours += self.check_alive(&Cell::new(x, y - 1)) as u8;
        neighbours += self.check_alive(&Cell::new(x + 1, y - 1)) as u8;

        neighbours += self.check_alive(&Cell::new(x - 1, y)) as u8;
        neighbours += self.check_alive(&Cell::new(x + 1, y)) as u8;

        neighbours += self.check_alive(&Cell::new(x - 1, y + 1)) as u8;
        neighbours += self.check_alive(&Cell::new(x, y + 1)) as u8;
        neighbours += self.check_alive(&Cell::new(x + 1, y + 1)) as u8;

        (cell_alive && (2..=3).contains(&(neighbours))) || neighbours == 3
    }

    #[inline(always)]
    pub fn check_alive(&self, cell: &Cell) -> bool {
        self.prev_cells.contains(cell)
    }

    pub fn next_gen(&mut self) {
        mem::swap(&mut self.cells, &mut self.prev_cells);
        self.cells.clear();

        self.prev_cells.par_iter().for_each(|cell| {
            ((cell.x - 1)..=(cell.x + 1)).into_par_iter().for_each(|x| {
                ((cell.y - 1)..=(cell.y + 1)).into_par_iter().for_each(|y| {
                    let pos = Cell::new(x, y);
                    if self.check_cell(&pos) {
                        self.cells.insert(pos);
                    }
                })
            });
        });
    }
}
