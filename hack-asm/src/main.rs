use std::{collections::HashMap, env, fs, mem, str::Chars};

#[derive(Debug, Clone)]
enum AInstr {
    Unresolved(String),
    Resolved(u16),
}

type CInstr = u16;

#[derive(Debug, Clone)]
enum Instr {
    A(AInstr),
    C(CInstr),
    Error(String),
}

fn is_symbol(c: char) -> bool {
    matches!(c, 'a'..='z' | 'A'..='Z' | '0'..='9' | '_' | '.' | '$' | ':')
}

struct Parser<'a> {
    symbols: HashMap<String, u16>,
    code_table: &'a HashMap<String, u8>,
    src: Chars<'a>,
    curr: char,
    literal: String,
    line: u16,
    instr_cnt: u16,
}

impl<'a> Parser<'a> {
    fn advance(&mut self) -> char {
        let prev = self.curr;
        self.literal.push(prev);
        self.curr = self.src.next().unwrap_or_default();
        prev
    }

    fn take_symbols(&mut self) -> HashMap<String, u16> {
        mem::take(&mut self.symbols)
    }

    fn peek(&self) -> char {
        self.curr
    }

    pub fn new(source: &'a str, code_table: &'a HashMap<String, u8>) -> Self {
        let mut src = source.chars();
        let curr = src.next().unwrap_or_default();

        let symbols: HashMap<String, u16> = HashMap::from(
            [
                ("R0", 0),
                ("R1", 1),
                ("R2", 2),
                ("R3", 3),
                ("R4", 4),
                ("R5", 5),
                ("R6", 6),
                ("R7", 7),
                ("R8", 8),
                ("R9", 9),
                ("R10", 10),
                ("R11", 11),
                ("R12", 12),
                ("R13", 13),
                ("R14", 14),
                ("R15", 15),
                ("SP", 0),
                ("LCL", 1),
                ("ARG", 2),
                ("THIS", 3),
                ("THAT", 4),
                ("SCREEN", 16384),
                ("KBD", 24576),
            ]
            .map(|(s, i)| (s.to_string(), i)),
        );

        Self {
            code_table,
            symbols,
            src,
            curr,
            literal: String::new(),
            line: 1,
            instr_cnt: 0,
        }
    }

    fn skip(&mut self) {
        self.curr = self.src.next().unwrap_or_default();
    }

    fn skip_whitespace(&mut self) {
        while self.peek() != '\0' {
            match self.peek() {
                ' ' | '\t' | '\r' => self.skip(),
                '\n' => {
                    self.line += 1;
                    self.skip();
                }
                '/' => {
                    self.skip();
                    if self.peek() == '/' {
                        while self.peek() != '\n' {
                            self.skip();
                        }
                    }
                }
                _ => break,
            }
        }
    }

    fn take_literal(&mut self) -> String {
        mem::take(&mut self.literal)
    }

    fn make_error(&mut self, msg: String) -> Instr {
        Instr::Error(format!("[LINE: {}] Error: '{}'", self.line, msg))
    }

    fn make_a_instr(&mut self, data: AInstr) -> Instr {
        Instr::A(data)
    }

    fn make_c_instr(&mut self, dest: u8, comp: u8, jump: u8) -> Instr {
        let mut instr: u16 = 0b1110000000000000;
        instr |= (comp as u16) << 6;
        instr |= (dest as u16) << 3;
        instr |= jump as u16;
        Instr::C(instr)
    }

    fn parse_a_instr(&mut self) -> Instr {
        match self.peek() {
            c if c.is_ascii_digit() => {
                self.advance();
                while self.peek().is_ascii_digit() {
                    self.advance();
                }

                let literal = self.take_literal();
                let parse_res: u16 = match literal.parse() {
                    Ok(x) => x,
                    Err(_) => {
                        return self.make_error(format!("Can't parse the '{}' literal", literal))
                    }
                };

                self.make_a_instr(AInstr::Resolved(parse_res))
            }
            c if is_symbol(c) => {
                self.advance();
                while !self.peek().is_ascii_whitespace() {
                    self.advance();
                }

                let literal = self.take_literal();
                self.make_a_instr(AInstr::Unresolved(literal))
            }
            c => self.make_error(format!("Unexpected symbol '{}'", c)),
        }
    }

    fn read_dest(&mut self) -> u8 {
        let mut dest = 0;
        let literal = self.take_literal();
        if literal.contains("M") {
            dest |= 0b001;
        }
        if literal.contains("D") {
            dest |= 0b010;
        }
        if literal.contains("A") {
            dest |= 0b100;
        }
        dest
    }

    fn read_comp(&mut self) -> Result<u8, String> {
        let literal = self.take_literal();
        match self.code_table.get(&literal) {
            Some(code) => Ok(*code),
            None => Err(format!("Invalid comp in c instruction '{}'", literal)),
        }
    }

    fn read_jump(&mut self) -> Result<u8, String> {
        let literal = self.take_literal();
        match self.code_table.get(&literal) {
            Some(code) => Ok(*code),
            None => Err(format!("Invalid jump in c instruction '{}'", literal)),
        }
    }

    fn parse_c_instr(&mut self) -> Instr {
        while !matches!(self.peek(), ' ' | '=' | ';' | '\n' | '\r') {
            self.advance();
        }

        let mut dest = 0;
        let mut comp = 0;
        let mut jump = 0;

        let mut comp_found = false;

        // dest or comp
        let is_dest = self.peek() == '=';
        if is_dest {
            dest = self.read_dest();
        } else {
            comp_found = true;
            match self.read_comp() {
                Ok(code) => comp = code,
                Err(e) => return self.make_error(e),
            }
        }
        if self.peek().is_ascii_whitespace() {
            self.skip();
            return self.make_c_instr(dest, comp, jump);
        }
        self.skip();

        // comp or jump
        while !matches!(self.peek(), ' ' | ';' | '\n' | '\r') {
            self.advance();
        }
        if !comp_found {
            match self.read_comp() {
                Ok(code) => comp = code,
                Err(e) => return self.make_error(e),
            }
        } else {
            match self.read_jump() {
                Ok(code) => jump = code,
                Err(e) => return self.make_error(e),
            };
            return self.make_c_instr(dest, comp, jump);
        }
        if self.peek().is_ascii_whitespace() {
            self.skip();
            return self.make_c_instr(dest, comp, jump);
        }
        self.skip();

        // jump
        while !matches!(self.peek(), ' ' | '=' | ';' | '\n' | '\r') {
            self.advance();
        }
        match self.read_jump() {
            Ok(code) => jump = code,
            Err(e) => return self.make_error(e),
        }
        self.make_c_instr(dest, comp, jump)
    }

    fn set_label(&mut self, symbol: String) -> bool {
        self.symbols.insert(symbol, self.instr_cnt).is_none()
    }

    fn parse_label(&mut self) -> Option<Instr> {
        while self.peek() != ')' {
            if !is_symbol(self.peek()) {
                return Some(self.make_error(format!("Invalid symbol '{}' in label", self.peek())));
            }

            if self.peek() == '\n' {
                return Some(self.make_error("Undelimited label".to_string()));
            }

            self.advance();
        }
        self.skip();

        let literal = self.take_literal();

        self.set_label(literal);

        None
    }
}

impl<'a> Iterator for Parser<'a> {
    type Item = Instr;
    fn next(&mut self) -> Option<Self::Item> {
        self.skip_whitespace();

        while self.peek() == '(' {
            self.skip();
            if let Some(err_instr) = self.parse_label() {
                return Some(err_instr);
            }
            self.skip_whitespace();
        }

        let c = self.peek();

        let instr = match c {
            '@' => {
                self.skip();
                self.parse_a_instr()
            }
            '\0' => return None,
            _ => self.parse_c_instr(),
        };

        self.instr_cnt += 1;

        Some(instr)
    }
}

fn compile(instrs: Vec<Instr>, mut symbols: HashMap<String, u16>) -> Vec<u16> {
    let mut var_cnt = 16;
    instrs
        .iter()
        .map(|instr| match instr {
            Instr::A(AInstr::Unresolved(symbol)) => match symbols.get(symbol) {
                Some(x) => Instr::A(AInstr::Resolved(*x)),
                None => {
                    symbols.insert(symbol.to_string(), var_cnt);
                    var_cnt += 1;
                    Instr::A(AInstr::Resolved(var_cnt - 1))
                }
            },
            _ => (*instr).clone(),
        })
        .map(|instr| match instr {
            Instr::A(AInstr::Resolved(x)) => 0b0000000000000000 | x,
            Instr::C(x) => x,
            err => panic!(": {:?}", err),
        })
        .collect()
}

fn main() {
    let mut args = env::args().skip(1);

    if args.len() != 1 {
        eprintln!("Error: no file provided");
    }
    let source_path = args.next().unwrap();

    let source = fs::read_to_string(&source_path).expect("Can't read the source file");

    let code_table: HashMap<String, u8> = HashMap::from(
        [
            // comp a=0
            ("0", 0b101010),
            ("1", 0b111111),
            ("-1", 0b111010),
            ("D", 0b001100),
            ("A", 0b110000),
            ("!D", 0b001101),
            ("!A", 0b110001),
            ("-D", 0b001111),
            ("-A", 0b110011),
            ("D+1", 0b011111),
            ("1+D", 0b011111),
            ("A+1", 0b110111),
            ("1+A", 0b110111),
            ("D-1", 0b001110),
            ("A-1", 0b110010),
            ("D+A", 0b000010),
            ("A+D", 0b000010),
            ("D-A", 0b010011),
            ("A-D", 0b000111),
            ("D&A", 0b000000),
            ("A&D", 0b000000),
            ("D|A", 0b010101),
            // comp a=1
            ("M", 0b1110000),
            ("!M", 0b1110001),
            ("-M", 0b1110011),
            ("M+1", 0b1110111),
            ("1+M", 0b1110111),
            ("M-1", 0b1110010),
            ("D+M", 0b1000010),
            ("M+D", 0b1000010),
            ("D-M", 0b1010011),
            ("M-D", 0b1000111),
            ("D&M", 0b1000000),
            ("M&D", 0b1000000),
            ("D|M", 0b1010101),
            // jump
            ("JGT", 0b001),
            ("JEQ", 0b010),
            ("JGE", 0b011),
            ("JLT", 0b100),
            ("JNE", 0b101),
            ("JLE", 0b110),
            ("JMP", 0b111),
        ]
        .map(|(s, i)| (s.to_string(), i)),
    );

    let mut lexer = Parser::new(&source, &code_table);

    let mut instrs = Vec::new();

    while let Some(instr) = lexer.next() {
        instrs.push(instr);
    }

    let symbols = lexer.take_symbols();

    let hack = compile(instrs, symbols);

    for x in hack {
        println!("{:016b}", x);
    }
}
