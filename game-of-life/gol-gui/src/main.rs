use ggez::graphics::MeshBatch;
use ggez::timer;
use ggez::{event, graphics};
use ggez::{Context, GameResult};
use gol_core::GameOfLife;

const DEFAULT_CELL_SIZE: f32 = 10.0;
const DEFAULT_BORDER_LINE_WIDTH: f32 = 2.0;
const DEFAULT_CELL_SIZE_HALF: f32 = DEFAULT_CELL_SIZE / 2.0;
const DEFAULT_FULL_CELL_SIZE: f32 = DEFAULT_CELL_SIZE + DEFAULT_BORDER_LINE_WIDTH;

fn clap(x: f32, min: f32, max: f32) -> f32 {
    if x < min {
        return min;
    }
    if max < x {
        return max;
    }

    x
}

struct GameState {
    gol: GameOfLife,
    pause: bool,
    left_mb_pressed: bool,
    middle_mb_pressed: bool,
    zoom: f32,
    x: f32,
    y: f32,
    m_x: f32,
    m_y: f32,
    ctrl_key_pressed: bool,
    game_speed: u32,
    cell_batch: MeshBatch,
}

impl GameState {
    fn new(ctx: &mut Context) -> Self {
        let gol = GameOfLife::new();
        let cell_batch = MeshBatch::new(
            graphics::Mesh::new_rectangle(
                ctx,
                graphics::DrawMode::fill(),
                graphics::Rect::new(0.0, 0.0, DEFAULT_CELL_SIZE, DEFAULT_CELL_SIZE),
                (0xFA, 0xBD, 0x2F).into(),
            )
            .unwrap(),
        )
        .unwrap();

        Self {
            gol,
            pause: true,
            left_mb_pressed: false,
            middle_mb_pressed: false,
            zoom: 1.0,
            x: 0.0,
            y: 0.0,
            m_x: 0.0,
            m_y: 0.0,
            ctrl_key_pressed: false,
            game_speed: 10,
            cell_batch,
        }
    }

    fn zoom_at(&mut self, x: f32, y: f32, zoom_by: f32) {
        self.zoom *= zoom_by;
        self.x = x - (x - self.x) * zoom_by;
        self.y = y - (y - self.y) * zoom_by;
    }

    fn set_cell(&mut self, x: f32, y: f32) {
        let grid_x =
            ((x + DEFAULT_CELL_SIZE_HALF - self.x) / DEFAULT_FULL_CELL_SIZE / self.zoom) as i32;

        let grid_y =
            ((y + DEFAULT_CELL_SIZE_HALF - self.y) / DEFAULT_FULL_CELL_SIZE / self.zoom) as i32;

        {
            let ref mut this = self.gol;
            niverse.set_cell(grid_x);

            let ref mut this = self.gol;
            this.universe.set_cell(grid_x);
        };
    }
}

impl event::EventHandler for GameState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        while timer::check_update_time(ctx, self.game_speed) {
            if !self.pause {
                self.gol.next_gen();
            }
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::clear(ctx, (28, 28, 28).into());
        self.cell_batch.clear();

        for cell in self.gol.iter() {
            let viewport_x =
                cell.x as f32 * DEFAULT_FULL_CELL_SIZE * self.zoom + self.x * self.zoom;
            let viewport_y =
                cell.y as f32 * DEFAULT_FULL_CELL_SIZE * self.zoom + self.y * self.zoom;
            let _viewport_cell_wh = DEFAULT_CELL_SIZE;

            // let rect_cell = graphics::Mesh::new_rectangle(
            //     ctx,
            //     graphics::DrawMode::fill(),
            //     graphics::Rect::new(viewport_x, viewport_y, viewport_cell_wh, viewport_cell_wh),
            //     (0xFA, 0xBD, 0x2F).into(),
            // )?;

            self.cell_batch.add((
                ggez::mint::Vector2 {
                    x: viewport_x,
                    y: viewport_y,
                },
                (0xFA, 0, 0).into(),
            ));

            self.cell_batch
                .draw(ctx, (ggez::mint::Point2 { x: 0.0, y: 0.0 },).into())?;
        }

        graphics::set_window_title(ctx, &format!("Game Of Life - {:.0} FPS", timer::fps(ctx),));
        graphics::present(ctx)?;
        Ok(())
    }

    fn mouse_button_down_event(
        &mut self,
        _ctx: &mut Context,
        button: event::MouseButton,
        x: f32,
        y: f32,
    ) {
        match button {
            event::MouseButton::Left => {
                println!("{} {}", x, y);
                self.left_mb_pressed = true;
                self.set_cell(x, y);
            }
            event::MouseButton::Right => {
                self.gol.next_gen();
            }
            event::MouseButton::Middle => {
                self.middle_mb_pressed = true;
            }
            _ => {}
        }
    }

    fn mouse_button_up_event(
        &mut self,
        _ctx: &mut Context,
        button: event::MouseButton,
        _x: f32,
        _y: f32,
    ) {
        match button {
            event::MouseButton::Left => {
                self.left_mb_pressed = false;
            }
            event::MouseButton::Middle => {
                self.middle_mb_pressed = false;
            }
            _ => {}
        }
    }

    fn mouse_motion_event(&mut self, _ctx: &mut Context, x: f32, y: f32, dx: f32, dy: f32) {
        if self.left_mb_pressed {
            self.set_cell(x, y);
        }
        if self.middle_mb_pressed {
            self.x += dx;
            self.y += dy;
        }
        self.m_x = x;
        self.m_y = y;
    }

    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        keycode: event::KeyCode,
        _keymods: event::KeyMods,
        _repeat: bool,
    ) {
        match keycode {
            event::KeyCode::Space => {
                self.pause ^= true;
            }
            event::KeyCode::LControl | event::KeyCode::RControl => {
                self.ctrl_key_pressed = true;
            }
            _ => {}
        }
    }

    fn key_up_event(
        &mut self,
        _ctx: &mut Context,
        keycode: event::KeyCode,
        _keymods: event::KeyMods,
    ) {
        match keycode {
            event::KeyCode::LControl | event::KeyCode::RControl => {
                self.ctrl_key_pressed = false;
            }
            _ => {}
        }
    }

    fn text_input_event(&mut self, _ctx: &mut Context, character: char) {
        match character {
            '+' => {
                if self.game_speed + 5 <= 120 {
                    self.game_speed += 5;
                }
            }
            '-' => {
                if 1 <= self.game_speed - 5 {
                    self.game_speed -= 5;
                }
            }
            _ => {}
        }
    }

    fn mouse_wheel_event(&mut self, _ctx: &mut Context, x: f32, y: f32) {
        if self.ctrl_key_pressed {
            let zoom = if 0.0 <= y {
                1.0 + clap(y, 0.1, 0.4)
            } else {
                1.0 + clap(y, -0.4, -0.1)
            };
            self.zoom_at(self.m_x, self.m_y, zoom);
        } else {
            self.x -= x;
            self.y += y;
        }
    }

    fn resize_event(&mut self, ctx: &mut Context, width: f32, height: f32) {
        let new_window_rect = graphics::Rect::new(0.0, 0.0, width, height);
        graphics::set_screen_coordinates(ctx, new_window_rect).unwrap();
    }
}

fn main() -> GameResult {
    let c = ggez::conf::Conf::new();
    let (mut ctx, game_loop) = ggez::ContextBuilder::new("Game of Life", "Blad")
        .window_setup(
            ggez::conf::WindowSetup::default()
                .vsync(false)
                .title("Game of Life"),
        )
        .window_mode(
            ggez::conf::WindowMode::default()
                .resizable(true)
                .maximized(true)
                .borderless(true)
                .fullscreen_type(ggez::conf::FullscreenType::Windowed),
        )
        .default_conf(c)
        .build()?;

    let state = GameState::new(&mut ctx);

    event::run(ctx, game_loop, state)
}
