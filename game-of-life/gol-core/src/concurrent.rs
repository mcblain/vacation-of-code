use fnv::FnvHashSet;
use rayon::prelude::*;
use std::mem;

const DEFAULT_CELLS_AMOUNT: usize = 100;

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Cell {
    pub x: i32,
    pub y: i32,
}

impl Cell {
    pub fn new(x: i32, y: i32) -> Self {
        Self { x, y }
    }
}

pub struct Universe {
    prev_cells: FnvHashSet<Cell>,
    cells: FnvHashSet<Cell>,
}

impl Default for Universe {
    fn default() -> Self {
        Self::new()
    }
}

impl Universe {
    pub fn set_cell(&mut self, cell: Cell) {
        self.cells.insert(cell);
    }

    pub fn remove_cell(&mut self, cell: &Cell) {
        self.cells.remove(cell);
    }

    fn check_cell(&self, cell: &Cell) -> bool {
        let mut neighbours = 0;

        for ix in (cell.x - 1)..=(cell.x + 1) {
            for iy in (cell.y - 1)..=(cell.y + 1) {
                if ix == cell.x && iy == cell.y {
                    continue;
                }

                if self.check_alive(&Cell::new(ix, iy)) {
                    neighbours += 1;
                }
            }
        }

        if self.check_alive(cell) {
            (2..=3).contains(&neighbours)
        } else {
            neighbours == 3
        }
    }

    pub fn check_alive(&self, cell: &Cell) -> bool {
        self.prev_cells.contains(cell)
    }

    pub fn new() -> Self {
        Self {
            prev_cells: FnvHashSet::default(),
            cells: FnvHashSet::default(),
        }
    }

    pub fn next_gen(&mut self) {
        mem::swap(&mut self.cells, &mut self.prev_cells);
        self.cells.clear();

        let (cells, _) = self
            .prev_cells
            .par_iter()
            .fold(
                || (FnvHashSet::<Cell>::default(), FnvHashSet::default()),
                |(mut cells, mut checked), cell| {
                    for x in (cell.x - 1)..=(cell.x + 1) {
                        for y in (cell.y - 1)..=(cell.y + 1) {
                            let pos = Cell::new(x, y);

                            if checked.contains(&pos) {
                                continue;
                            }

                            if self.check_cell(&pos) {
                                cells.insert(pos.clone());
                            } else {
                                cells.remove(&pos);
                            }

                            checked.insert(pos);
                        }
                    }

                    return (cells, checked);
                },
            )
            .reduce(
                || (FnvHashSet::<Cell>::default(), FnvHashSet::<Cell>::default()),
                |(mut cells, checked), (c, _)| {
                    cells.clone_from(&c);
                    (cells, checked)
                },
            );

        self.cells = cells;

        // for cell in &self.prev_cells {
        //     for x in (cell.x - 1)..=(cell.x + 1) {
        //         for y in (cell.y - 1)..=(cell.y + 1) {
        //             let pos = Cell::new(x, y);

        //             if self.checked_cells.contains(&pos) {
        //                 continue;
        //             }

        //             if self.check_cell(&pos) {
        //                 self.cells.insert(pos.clone());
        //             } else {
        //                 self.cells.remove(&pos);
        //             }

        //             self.checked_cells.insert(pos);
        //         }
        //     }
        // }
    }
}

pub struct GameOfLife {
    universe: Universe,
}

impl Default for GameOfLife {
    fn default() -> Self {
        Self::new()
    }
}

impl GameOfLife {
    pub fn new() -> Self {
        GameOfLife {
            universe: Universe::new(),
        }
    }

    pub fn next_gen(&mut self) {
        self.universe.next_gen();
    }

    pub fn cells(&self) -> &FnvHashSet<Cell> {
        &self.universe.cells
    }

    pub fn remove_cell(&mut self, cell: &Cell) {
        self.universe.remove_cell(cell);
    }

    pub fn set_cell(&mut self, cell: Cell) {
        self.universe.set_cell(cell);
    }
}
