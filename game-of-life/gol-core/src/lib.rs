use std::mem;
pub mod cell;
pub mod concurrent;
use cell::{Cell, Cells};

pub struct GameOfLife {
    cells: Cells,
    prev_cells: Cells,
}

impl Default for GameOfLife {
    fn default() -> Self {
        Self::new()
    }
}

impl GameOfLife {
    pub fn new() -> Self {
        GameOfLife {
            prev_cells: Cells::new(),
            cells: Cells::new(),
        }
    }

    pub fn cells(&self) -> impl Iterator<Item = Cell> + '_ {
        self.cells.get()
    }

    pub fn set_cell(&mut self, cell: Cell) {
        self.cells.insert(cell);
    }

    pub fn remove_cell(&mut self, _cell: &Cell) {
        // self.cells.remove(cell);
    }

    fn check_cell(&self, cell: &Cell) -> bool {
        let mut neighbours = 0;
        let cell_alive = self.check_alive(cell) as u8;

        for ix in (cell.x - 1)..=(cell.x + 1) {
            for iy in (cell.y - 1)..=(cell.y + 1) {
                neighbours += self.check_alive(&Cell::new(ix, iy)) as u8;
            }
        }

        (cell_alive == 1 && (2..=3).contains(&(neighbours - 1))) || neighbours - cell_alive == 3
    }

    pub fn check_alive(&self, cell: &Cell) -> bool {
        self.prev_cells.contains(cell)
    }

    pub fn iter(&self) -> impl Iterator + '_ {
        self.cells.get()
    }

    pub fn next_gen(&mut self) {
        mem::swap(&mut self.cells, &mut self.prev_cells);
        self.cells.clear();
        for cell in self.prev_cells.get() {
            for x in (cell.x - 1)..=(cell.x + 1) {
                for y in (cell.y - 1)..=(cell.y + 1) {
                    let pos = Cell::new(x, y);
                    if self.check_cell(&pos) {
                        self.cells.insert(pos);
                    }
                }
            }
        }
    }
    // pub fn with_capacity(capacity: usize) -> Self {
    //     Self {
    //         cells: FnvHashSet::with_capacity(capacity),
    //         prev_cells: FnvHashSet::with_capacity(capacity),
    //         checked_cells: FnvHashSet::with_capacity(capacity),
    //     }
    // }
}
