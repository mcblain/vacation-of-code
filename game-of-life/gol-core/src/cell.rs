const DEFAULT_CAPACITY: usize = 16;

pub type Coord = i16;

#[derive(Clone, PartialEq, Eq, Hash, Default, Copy)]
pub struct Cell {
    pub x: Coord,
    pub y: Coord,
}

impl Cell {
    pub fn new(x: Coord, y: Coord) -> Self {
        Self { x, y }
    }
}

pub struct Cells {
    pub vec: Vec<Option<Cell>>,
    pub len: usize,
}

impl Default for Cells {
    fn default() -> Self {
        Self {
            vec: vec![None; DEFAULT_CAPACITY],
            len: 0,
        }
    }
}

impl Cells {
    pub fn new() -> Self {
        Cells::default()
    }

    pub fn get(&self) -> impl Iterator<Item = Cell> + '_ {
        self.vec
            .iter()
            .filter_map(|val_opt| val_opt.as_ref().copied())
    }

    pub fn resize(&mut self, size: usize) {
        let min_len = if size < self.vec.len() {
            size
        } else {
            self.vec.len()
        };

        let mut new_vec: Vec<Option<Cell>> = vec![None; size];
        self.len = 0;

        for i in 0..min_len {
            let cell = if let Some(val) = self.vec[i] {
                val
            } else {
                continue;
            };
            let mut new_pos = (cell.x + cell.y) as usize % size;

            while new_vec[new_pos].is_some() {
                new_pos += 1;
                new_pos %= size;
            }

            new_vec[new_pos] = Some(cell);
            self.len += 1;
        }

        self.vec = new_vec;
    }

    pub fn insert(&mut self, cell: Cell) -> bool {
        if self.len == self.vec.len() {
            self.resize(self.vec.len() * 2);
        }

        let mut pos = (cell.x + cell.y) as usize % self.vec.len();

        while let Some(vec_cell) = self.vec[pos] {
            if cell == vec_cell {
                return false;
            }

            pos += 1;
            pos %= self.vec.len();
        }

        self.vec[pos] = Some(cell);
        self.len += 1;
        true
    }

    pub fn remove() {}

    pub fn contains(&self, cell: &Cell) -> bool {
        let mut pos = (cell.x + cell.y) as usize % self.vec.len();
        let end_pos = pos.wrapping_sub(1) % self.vec.len();

        while let Some(vec_cell) = self.vec[pos] {
            if *cell == vec_cell {
                return true;
            }

            if pos == end_pos {
                return false;
            }

            pos += 1;
            pos %= self.vec.len();
        }

        false
    }

    pub fn clear(&mut self) {
        self.vec.fill(None);
    }
}
