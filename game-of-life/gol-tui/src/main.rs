use crossterm::event::{
    Event, KeyCode, KeyEvent, KeyModifiers, MouseButton, MouseEvent, MouseEventKind,
};
use crossterm::{cursor, event, style, terminal};
use crossterm::{execute, queue};
use fnv::FnvHashSet;
use gol_core::cell::{Cell, Coord};
use gol_core::GameOfLife;
use std::io::Write;
use std::{io, time};

const FPS_LIMIT: u32 = 120;
const GAME_TICKS_PER_SEC: u32 = 10;

type Pixel = (u16, u16);

type Buffer = FnvHashSet<Pixel>;

struct Viewport {
    w: u16,
    h: u16,
    x: Coord,
    y: Coord,
    buffer: Buffer,
    ctx: std::io::Stdout,
}

impl Viewport {
    pub fn drag(&mut self, x_offset: i32, y_offset: i32) {
        self.x += x_offset as Coord;
        self.y += y_offset as Coord;
    }

    pub fn get_cell_universe_pos(&self, x: u16, y: u16) -> Cell {
        Cell::new(self.x + x as Coord, self.y + y as Coord)
    }

    pub fn restore(&mut self) {
        execute!(
            self.ctx,
            terminal::Clear(terminal::ClearType::All),
            terminal::LeaveAlternateScreen,
            cursor::Show,
        )
        .unwrap();
        terminal::disable_raw_mode().unwrap();
    }

    pub fn get_cell_vp_pos(&self, cell: &Cell) -> Option<Pixel> {
        let vp_x = cell.x - self.x;
        let vp_y = cell.y - self.y;
        let width = self.w as i16;
        let height = self.h as i16;

        if 0 <= vp_x && vp_x < width && 0 < vp_y && vp_y < height {
            return Some((vp_x as u16, vp_y as u16));
        }

        None
    }

    pub fn new(mut ctx: io::Stdout) -> Result<Self, io::Error> {
        terminal::enable_raw_mode()?;

        execute!(
            ctx,
            terminal::EnterAlternateScreen,
            terminal::Clear(terminal::ClearType::All),
            event::EnableMouseCapture,
            style::SetForegroundColor(style::Color::Yellow),
            cursor::Hide,
        )?;

        let (width, height) = terminal::size().expect("Cant find viewport size");

        Ok(Self {
            w: width,
            h: height,
            x: 0,
            y: 0,
            buffer: Buffer::default(),
            ctx,
        })
    }

    pub fn resize(&mut self, w: u16, h: u16) {
        self.w = w;
        self.h = h;
    }

    pub fn draw_cells(&mut self, cells: impl Iterator<Item = Cell>) -> Result<(), io::Error> {
        let mut cells_buff = Buffer::default();

        for cell in cells {
            match self.get_cell_vp_pos(&cell) {
                Some((vp_x, vp_y)) => cells_buff.insert((vp_x, vp_y)),
                None => continue,
            };
        }

        self.update(&cells_buff)?;

        Ok(())
    }

    pub fn update(&mut self, upd_buff: &Buffer) -> Result<(), io::Error> {
        for pixel in &self.buffer {
            let (prev_x, prev_y) = *pixel;

            if !upd_buff.contains(&(prev_x, prev_y)) {
                queue!(self.ctx, cursor::MoveTo(prev_x, prev_y), style::Print(' '),)?;
            }
        }

        self.buffer.clear();

        for pixel in upd_buff {
            let (x, y) = *pixel;
            self.buffer.insert((x, y));
            queue!(self.ctx, cursor::MoveTo(x, y), style::Print('\u{25A0}'))?;
        }

        self.ctx.flush()?;

        Ok(())
    }
}

fn run(mut vp: Viewport, mut gol: GameOfLife) -> Result<(), io::Error> {
    let mut pause = true;
    let mut prev_drag_pos = (u16::MAX, u16::MAX);
    let mut updated = false;
    let mut tick_timestamp = time::Instant::now();
    let mut fps_timestamp = time::Instant::now();
    let mut tick_elapsed;
    let mut fps_elapsed;
    let mut game_ticks_per_sec = GAME_TICKS_PER_SEC;
    let fps_limit = FPS_LIMIT;
    let mut tick_duration = time::Duration::from_micros(1_000_000 / game_ticks_per_sec as u64);
    let frame_render_duration = time::Duration::from_micros(1_000_000 / fps_limit as u64);

    loop {
        tick_elapsed = tick_timestamp.elapsed();
        if tick_duration <= tick_elapsed {
            if !pause {
                gol.next_gen();
                updated = true;
            }
            tick_timestamp = time::Instant::now();
        }

        fps_elapsed = fps_timestamp.elapsed();
        if frame_render_duration <= fps_elapsed {
            if updated {
                updated = false;
                vp.draw_cells(gol.cells())?;
            }

            if event::poll(time::Duration::from_millis(0))? {
                match event::read()? {
                    Event::Key(KeyEvent { code, modifiers }) => match code {
                        KeyCode::Char('c') if modifiers == KeyModifiers::CONTROL => {
                            vp.restore();
                            return Ok(());
                        }
                        KeyCode::Char(' ') => {
                            pause = !pause;
                        }
                        KeyCode::Char('+') => {
                            game_ticks_per_sec += 10;
                            tick_duration =
                                time::Duration::from_micros(1_000_000 / game_ticks_per_sec as u64);
                        }
                        KeyCode::Char('-') => {
                            if 4 <= game_ticks_per_sec {
                                game_ticks_per_sec -= 2;
                                tick_duration = time::Duration::from_micros(
                                    1_000_000 / game_ticks_per_sec as u64,
                                );
                            }
                        }

                        _ => continue,
                    },
                    Event::Mouse(MouseEvent {
                        kind,
                        column: vp_x,
                        row: vp_y,
                        ..
                    }) => match kind {
                        MouseEventKind::Drag(MouseButton::Left)
                        | MouseEventKind::Down(MouseButton::Left) => {
                            let cell = vp.get_cell_universe_pos(vp_x, vp_y);
                            gol.set_cell(cell);
                            updated = true;
                        }
                        MouseEventKind::Drag(MouseButton::Right)
                        | MouseEventKind::Down(MouseButton::Right) => {
                            let cell = vp.get_cell_universe_pos(vp_x, vp_y);
                            gol.remove_cell(&cell);
                            updated = true;
                        }
                        MouseEventKind::Drag(MouseButton::Middle) => match prev_drag_pos {
                            (u16::MAX, u16::MAX) => {
                                prev_drag_pos = (vp_x, vp_y);
                            }
                            (prev_x, prev_y) => {
                                vp.drag(prev_x as i32 - vp_x as i32, prev_y as i32 - vp_y as i32);
                                prev_drag_pos = (vp_x, vp_y);
                                updated = true;
                            }
                        },

                        MouseEventKind::Up(MouseButton::Middle) => {
                            prev_drag_pos = (u16::MAX, u16::MAX);
                        }
                        _ => continue,
                    },
                    Event::Resize(w, h) => {
                        vp.resize(w, h);
                        updated = true
                    }
                }

                while event::poll(time::Duration::from_millis(0))? {
                    event::read()?;
                }
            }

            fps_timestamp = time::Instant::now();
        }
        let next_frame_time = frame_render_duration.checked_sub(fps_timestamp.elapsed());
        let next_tick_time = tick_duration.checked_sub(tick_timestamp.elapsed());
        match (next_frame_time, next_tick_time) {
            (Some(x), _) if pause => std::thread::sleep(x),
            (Some(x), Some(y)) => std::thread::sleep(if x < y { x } else { y }),
            (None, _) => {}
            (_, None) => {}
        };
    }
}

fn main() {
    let ctx = io::stdout();
    let gol = GameOfLife::new();

    let vp = match Viewport::new(ctx) {
        Ok(vp) => vp,
        Err(_) => {
            return;
        }
    };

    run(vp, gol).unwrap();
}
