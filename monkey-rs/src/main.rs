mod debug;
mod lexer;
mod repl;
mod token;

fn main() {
    let args = std::env::args().skip(1);

    if args.count() == 0 {
        repl::run();
    }
}
