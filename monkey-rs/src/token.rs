use std::{fmt, mem};

pub type TokenCode = u8;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum TokenKind {
    Illegal,
    Eof,
    // identifiers + literals
    Ident,
    Int,
    // operators
    Assign,
    Plus,
    Minus,
    Bang,
    Asterisk,
    Slash,
    Lt,
    Gt,
    Eq,
    NotEq,
    // delimiters
    Comma,
    Semicolon,
    LParen,
    RParen,
    LBrace,
    RBrace,
    // keywords
    Function,
    Let,
    True,
    False,
    If,
    Else,
    Return,
}

static TOKEN_TO_STR_MAP: [&str; 27] = {
    use TokenKind::*;
    let mut arr: [&str; 27] = [""; 27];
    arr[Illegal as usize] = "Illegal";
    arr[Eof as usize] = "Eof";
    // identifiers + literals
    arr[Ident as usize] = "Ident";
    arr[Int as usize] = "Int";
    // operators
    arr[Assign as usize] = "=";
    arr[Plus as usize] = "+";
    arr[Minus as usize] = "-";
    arr[Bang as usize] = "!";
    arr[Asterisk as usize] = "*";
    arr[Slash as usize] = "/";
    arr[Lt as usize] = "<";
    arr[Gt as usize] = ">";
    arr[Eq as usize] = "==";
    arr[NotEq as usize] = "!=";
    // delimiters
    arr[Comma as usize] = ",";
    arr[Semicolon as usize] = ";";
    arr[LParen as usize] = "(";
    arr[RParen as usize] = ")";
    arr[LBrace as usize] = "{";
    arr[RBrace as usize] = "}";
    // keywords
    arr[Function as usize] = "fn";
    arr[Let as usize] = "let";
    arr[True as usize] = "true";
    arr[False as usize] = "false";
    arr[If as usize] = "if";
    arr[Else as usize] = "else";
    arr[Return as usize] = "return";
    arr
};

impl From<TokenCode> for TokenKind {
    fn from(code: TokenCode) -> Self {
        unsafe { mem::transmute::<TokenCode, Self>(code) }
    }
}

impl From<TokenKind> for TokenCode {
    fn from(kind: TokenKind) -> Self {
        kind as TokenCode
    }
}

impl From<TokenKind> for usize {
    fn from(kind: TokenKind) -> Self {
        kind as usize
    }
}

impl fmt::Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let str_repr: &str = TOKEN_TO_STR_MAP[*self as usize];
        write!(f, "{}", str_repr)
    }
}

#[derive(Debug)]
pub struct Token {
    pub kind: TokenKind,
    pub literal: String,
    pub line: usize,
}

impl Token {
    pub fn new(kind: TokenKind, literal: String, line: usize) -> Self {
        Self {
            kind,
            literal,
            line,
        }
    }
}
