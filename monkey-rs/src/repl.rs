use crate::{lexer::Lexer, token::TokenKind};
use std::io;
use std::io::Write;

const PROMPT: &str = ">> ";

pub fn run() {
    let stdin = io::stdin();
    let mut stdout = io::stdout();
    let mut buff = String::new();

    loop {
        print!("{}", PROMPT);
        stdout.flush().unwrap_or_default();
        buff.clear();
        match stdin.read_line(&mut buff) {
            Ok(len) => {
                if len == 0 {
                    return;
                }
            }
            Err(err) => {
                eprintln!("ERROR(repl): {}", err);
                return;
            }
        }
        let lexer = Lexer::new(&buff);
        for token in lexer {
            if token.kind != TokenKind::Eof {
                println!("{:?}", token);
            }
        }
    }
}
