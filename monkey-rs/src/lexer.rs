use crate::token::{Token, TokenKind};

use std::{
    iter::{self, Peekable},
    mem,
    str::Chars,
};

pub struct Lexer<'a> {
    source: Peekable<Chars<'a>>,
    literal: String,
    line: usize,
    curr: char,
    finished: bool,
}

fn is_alpha(c: &char) -> bool {
    c.is_ascii_alphabetic()
}

fn is_digit(c: &char) -> bool {
    c.is_ascii_digit()
}

impl<'a> Lexer<'a> {
    pub fn new(source: &'a String) -> Self {
        if source.is_empty() {
            eprintln!("Error: Lexer source is empty");
        };

        let mut chars = source.chars().peekable();
        let curr = chars.next().unwrap_or_default();

        Self {
            source: chars,
            curr,
            literal: String::new(),
            line: 1,
            finished: false,
        }
    }

    fn is_at_end(&mut self) -> bool {
        self.source.peek().is_none()
    }

    fn advance(&mut self) -> char {
        let prev = self.curr;
        self.literal.push(prev);
        self.curr = self.source.next().unwrap_or_default();
        prev
    }

    fn make_token(&mut self, kind: TokenKind) -> Token {
        Token::new(kind, mem::take(&mut self.literal), self.line)
    }

    fn peek(&mut self) -> char {
        self.curr
    }

    fn peek_next(&mut self) -> char {
        self.source.peek().copied().unwrap_or_default()
    }

    fn check_keyword(&self, start: usize, rest: &str, kind: TokenKind) -> TokenKind {
        if &self.literal[start..] == rest {
            kind
        } else {
            TokenKind::Ident
        }
    }

    fn lookup_ident(&self) -> TokenKind {
        use TokenKind::*;
        let mut chars = self.literal.chars();

        match chars.next().unwrap_or_default() {
            'f' => match chars.next().unwrap_or_default() {
                'n' => self.check_keyword(2, "", Function),
                'a' => self.check_keyword(2, "lse", False),
                _ => Ident,
            },
            'l' => self.check_keyword(1, "et", Let),
            't' => self.check_keyword(1, "rue", True),
            'i' => self.check_keyword(1, "f", If),
            'e' => self.check_keyword(1, "lse", Else),
            'r' => self.check_keyword(1, "eturn", Return),
            _ => Ident,
        }
    }

    fn read_ident(&mut self) -> Token {
        while is_alpha(&self.peek()) || is_digit(&self.peek()) {
            self.advance();
        }

        self.make_token(self.lookup_ident())
    }

    fn read_number(&mut self) -> Token {
        while self.peek().is_ascii_digit() {
            self.advance();
        }

        if self.peek() == '.' && self.peek_next().is_ascii_digit() {
            self.advance();
            self.advance();
            while self.peek().is_ascii_digit() {
                self.advance();
            }
            unimplemented!("Floating point numbers are currently unimplemented");
            // self.make_token(TokenKind::Float)
        } else {
            self.make_token(TokenKind::Int)
        }
    }

    fn skip(&mut self) {
        self.curr = self.source.next().unwrap_or_default();
    }

    fn nskip(&mut self, mut n: u8) {
        while 0 != n {
            self.skip();
            n -= 1;
        }
    }

    pub fn skip_whitespace(&mut self) {
        while self.peek() != '\0' {
            match self.peek() {
                ' ' | '\r' | '\t' => self.skip(),
                '\n' => {
                    self.line += 1;
                    self.skip();
                }
                '/' if '/' == self.peek_next() => {
                    self.nskip(2);
                    while self.peek() != '\n' && !self.is_at_end() {
                        self.skip();
                    }
                    self.skip();
                }
                _ => return,
            };
        }
    }

    fn expect(&mut self, c: char) -> bool {
        if self.peek() == c {
            self.advance();
            true
        } else {
            false
        }
    }
}

impl<'a> iter::Iterator for Lexer<'a> {
    type Item = Token;
    fn next(&mut self) -> Option<Self::Item> {
        use TokenKind::*;

        if self.finished {
            return None;
        }

        self.skip_whitespace();

        let c = self.advance();
        if is_alpha(&c) {
            return Some(self.read_ident());
        }
        if c.is_ascii_digit() {
            return Some(self.read_number());
        }
        let token = match c {
            '=' if self.expect('=') => self.make_token(Eq),
            '=' => self.make_token(Assign),
            '+' => self.make_token(Plus),
            '-' => self.make_token(Minus),
            '!' if self.expect('=') => self.make_token(NotEq),
            '!' => self.make_token(Bang),
            '/' => self.make_token(Slash),
            '*' => self.make_token(Asterisk),
            '<' => self.make_token(Lt),
            '>' => self.make_token(Gt),
            ';' => self.make_token(Semicolon),
            '(' => self.make_token(LParen),
            ')' => self.make_token(RParen),
            ',' => self.make_token(Comma),
            '{' => self.make_token(LBrace),
            '}' => self.make_token(RBrace),
            '\0' => {
                self.finished = true;
                self.make_token(Eof)
            }
            _ => self.make_token(Illegal),
        };

        Some(token)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn next_token() {
        use TokenKind::*;

        let input = "let five = 5;
        let ten = 10;

        let add = fn(x, y) {
            x + y;
        };
        
        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;
            
        if (5 < 10) {
            return true;
        } else {
            return false;
        }

        10 == 10;
        10 != 9;
        "
        .to_string();

        let expected_tokens = [
            // 0
            Token::new(Let, "let".to_string(), 0),
            Token::new(Ident, "five".to_string(), 0),
            Token::new(Assign, "=".to_string(), 0),
            Token::new(Int, "5".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(Let, "let".to_string(), 0),
            Token::new(Ident, "ten".to_string(), 0),
            Token::new(Assign, "=".to_string(), 0),
            Token::new(Int, "10".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            // 10
            Token::new(Let, "let".to_string(), 0),
            Token::new(Ident, "add".to_string(), 0),
            Token::new(Assign, "=".to_string(), 0),
            Token::new(Function, "fn".to_string(), 0),
            Token::new(LParen, "(".to_string(), 0),
            Token::new(Ident, "x".to_string(), 0),
            Token::new(Comma, ",".to_string(), 0),
            Token::new(Ident, "y".to_string(), 0),
            Token::new(RParen, ")".to_string(), 0),
            Token::new(LBrace, "{".to_string(), 0),
            // 20
            Token::new(Ident, "x".to_string(), 0),
            Token::new(Plus, "+".to_string(), 0),
            Token::new(Ident, "y".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(RBrace, "}".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(Let, "let".to_string(), 0),
            Token::new(Ident, "result".to_string(), 0),
            Token::new(Assign, "=".to_string(), 0),
            Token::new(Ident, "add".to_string(), 0),
            // 30
            Token::new(LParen, "(".to_string(), 0),
            Token::new(Ident, "five".to_string(), 0),
            Token::new(Comma, ",".to_string(), 0),
            Token::new(Ident, "ten".to_string(), 0),
            Token::new(RParen, ")".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(Bang, "!".to_string(), 0),
            Token::new(Minus, "-".to_string(), 0),
            Token::new(Slash, "/".to_string(), 0),
            Token::new(Asterisk, "*".to_string(), 0),
            // 40
            Token::new(Int, "5".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(Int, "5".to_string(), 0),
            Token::new(Lt, "<".to_string(), 0),
            Token::new(Int, "10".to_string(), 0),
            Token::new(Gt, ">".to_string(), 0),
            Token::new(Int, "5".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(If, "if".to_string(), 0),
            Token::new(LParen, "(".to_string(), 0),
            // 50
            Token::new(Int, "5".to_string(), 0),
            Token::new(Lt, "<".to_string(), 0),
            Token::new(Int, "10".to_string(), 0),
            Token::new(RParen, ")".to_string(), 0),
            Token::new(LBrace, "{".to_string(), 0),
            Token::new(Return, "return".to_string(), 0),
            Token::new(True, "true".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(RBrace, "}".to_string(), 0),
            Token::new(Else, "else".to_string(), 0),
            // 60
            Token::new(LBrace, "{".to_string(), 0),
            Token::new(Return, "return".to_string(), 0),
            Token::new(False, "false".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(RBrace, "}".to_string(), 0),
            Token::new(Int, "10".to_string(), 0),
            Token::new(Eq, "==".to_string(), 0),
            Token::new(Int, "10".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(Int, "10".to_string(), 0),
            Token::new(NotEq, "!=".to_string(), 0),
            Token::new(Int, "9".to_string(), 0),
            Token::new(Semicolon, ";".to_string(), 0),
            Token::new(Eof, "\0".to_string(), 0),
        ];

        let mut lexer = Lexer::new(&input);

        for (i, expected) in expected_tokens.iter().enumerate() {
            if let Some(token) = lexer.next() {
                assert_eq!(token.kind, expected.kind, "Test[{}]", i);
                assert_eq!(token.literal, expected.literal, "Test[{}]", i);
            }
        }
    }
}
