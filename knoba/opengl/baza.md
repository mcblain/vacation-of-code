graphics pipeline обычно состоит из следующих этапов:

* Vertex shader:
  Трансформирует одни 3D координаты в другие.

* Shape assembly:
  Берет вершины из предыдущего,
  группирует в примитив
  (points, triangles, strip)
  и передает его дальше.

* Geometry shader:
  Принимает вершины формирующие
  примитив, и может создавать
  новые вершины для создания
  новых примитивов.

* Rasterization:
  превращает 3D примитивы в пиксели.

* Fragment shader:
  Формирует финальный цвет пикселя.
  На этом этапе добавляются тени, освещение
  и т.д.

* Test and blending:
  Думает какой объект должен находиться ближе,
  а какой дальше. Также здесь применяются альфа
  каналы для создания эффекта прозрачности для
  объектов которые этого просят.
  
### normalized device coordinates:
  Координаты в формате [0.0 - 1.0].
  Означают координаты видимые на экране.
  
### VBO:
  Vertex Buffer Object - набор данных
  репрезентующих вершины объекта.
  Через VBO происходит передача данных в GPU.

### VAO
  Vertex Array Object - хранит в себе все
  настройки аттрибутов вершин, что
  позволяет сократить репетативное
  настраивание аттрибутов для каждого
  объекта сцены, и повысить производительность.

### EBO
  Entity Buffer Object - буффер хранящий индексы
  вершин в массиве вершин, для лучшего
  переиспользования существующих данных
  
### data management type
GL_STREAM_DRAW: the data is set only once and used by the GPU at most a few times.
GL_STATIC_DRAW: the data is set only once and used many times.
GL_DYNAMIC_DRAW: the data is changed a lot and used many times.

### Uniforms
  Uniforms - это еще один способ передачи данных
  в GPU. Юниформы глобальны - т.е. доступны
  из любого шейдера в шейдерной программе.
  Данные хранящиеся в Uniform, будут
  храниться там до следующей перезаписи
  или обновления.

### GLSL
  GLSL - язык написания шейдеров.
  
##### Структура
  ```glsl  
  #version version_number
  in type in_variable_name;
  in type in_variable_name;

  out type out_variable_name;

  uniform type uniform_name;

  void main()
  {
    // process input(s) and do some weird graphics stuff
    ...
    // output processed stuff to output variable
    out_variable_name = weird_stuff_we_processed;
  } ````

##### Vertex attribute
  Входящее значение для vertex шейдера.

  Максимальное Колличество аттрибутов для
  одного шейдера можно получить через запрос
  к **GL_MAX_VERTEX_ATTRIBS**

  ```c
  int nrAttributes;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
  printf("Maximum nr of vertex attributes supported: %d\n", nrAttributes) ```


##### Data types
  basic:
  - int
  - float
  - double
  - uint
  - bool

  vectors:
  - vecn: the default vector of n floats.
  - bvecn: a vector of n booleans.
  - ivecn: a vector of n integers.
  - uvecn: a vector of n unsigned integers.
  - dvecn: a vector of n double components.

  поля векторов получаются через точку: .x .y .z .w
  
  также есть особый синтаксис *swizzling* позволяющий
  обращаться сразу к нескольким полям с одновременной
  группировкой
  
  ```glsl
  vec2 someVec;
  vec4 differentVec = someVec.xyxx;
  vec3 anotherVec = differentVec.zyw;
  vec4 otherVec = someVec.xxxx + anotherVec.yxzy; ````
  
  Еще с помощью него можно делать так:

  ```glsl
  vec2 vect = vec2(0.5, 0.7);
  vec4 result = vec4(vect, 0.0, 0.0);
  vec4 otherResult = vec4(result.xyz, 1.0); ````
  


  
  
  
  
  
  
  
