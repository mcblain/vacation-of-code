#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <raylib.h>

typedef struct {
    float x;
    float y;
    int w;
    int h;
} Brick;

typedef struct {
    float x;
    float y;
    float vel_x;
    float vel_y;
    int r;
} Ball;

typedef enum { START, FINISH, IDLE } State;

typedef enum { LEFT, RIGHT, NONE } Winner;

void resize_brick(Brick* brick,
                  int old_scr_w,
                  int old_scr_h,
                  int new_scr_w,
                  int new_scr_h) {
    float relative_x = brick->x / (float)old_scr_w;
    float relative_y = brick->y / (float)old_scr_h;

    brick->x = relative_x * (float)new_scr_w;
    brick->y = relative_y * (float)new_scr_h;
    brick->w = (float)new_scr_w * 0.02f;
    brick->h = (float)new_scr_h * 0.20f;
}

void init_brick(Brick* brick, int scr_w, int scr_h, bool left) {
    float brick_offset = (float)scr_w / 20.0f;

    brick->w = (float)scr_w * 0.02f;
    brick->h = (float)scr_h * 0.20f;
    if (left) {
        brick->x = left * brick_offset;
    } else {
        brick->x = (float)scr_w - (float)brick->w - brick_offset;
    }
    brick->y = (float)scr_h / 2.0f - (brick->h / 2.0f);
}

void resize_ball(Ball* ball,
                 int old_scr_w,
                 int old_scr_h,
                 int new_scr_w,
                 int new_scr_h) {
    float relative_x = ball->x / (float)old_scr_w;
    float relative_y = ball->y / (float)old_scr_h;

    ball->r = (float)new_scr_h / 80;
    ball->x = relative_x * (float)new_scr_w;
    ball->y = relative_y * (float)new_scr_h;
}

void init_ball(Ball* ball, int scr_w, int scr_h) {
    ball->r = (float)scr_h / 80;
    ball->x = (float)scr_w / 2.0f - ball->r;
    ball->y = (float)scr_h / 2.0f - ball->r;
    ball->vel_x = 0.0f;
    ball->vel_y = 0.0f;
}

void clip_brick(Brick* brick, int scr_h) {
    if (brick->y < 0) {
        brick->y = 0;
    }

    if (scr_h < brick->y + brick->h) {
        brick->y = scr_h - brick->h;
    }
}

bool check_brick_collision(Ball* ball, Brick* brick) {
    float ball_left = ball->x;
    float ball_right = ball_left + ball->r;
    float ball_top = ball->y;
    float ball_bottom = ball_top + ball->r;

    float brick_left = brick->x;
    float brick_right = brick_left + brick->w;
    float brick_top = brick->y;
    float brick_bottom = brick_top + brick->h;

    return !(ball_right < brick_left || brick_right < ball_left ||
             ball_bottom < brick_top || brick_bottom < ball_top);
}

bool check_screen_collision(Ball* ball, int scr_w, int scr_h) {
    float left = ball->x;
    float right = left + ball->r;
    float top = ball->y;
    float bottom = top + ball->r;

    return left < 0 || scr_w < right || top < 0 || scr_h < bottom;
}

void update_ball(Ball* ball, Brick* left, Brick* right, int scr_w, int scr_h) {
    float vel_x = ball->vel_x;
    float vel_y = ball->vel_y;
    float x = ball->y;
    float y = ball->y;

    ball->x += vel_x * scr_w;

    if (check_screen_collision(ball, scr_w, scr_h) ||
        check_brick_collision(ball, left) ||
        check_brick_collision(ball, right)) {
        ball->x -= vel_x * scr_w;
        ball->vel_x *= -1;
    }

    ball->y += vel_y * scr_h;

    if (check_screen_collision(ball, scr_w, scr_h) ||
        check_brick_collision(ball, left) ||
        check_brick_collision(ball, right)) {
        ball->y -= vel_y * scr_h;
        ball->vel_y *= -1;
    }
}

Winner check_winner(Ball* ball, Brick* left, Brick* right) {
    int ball_left = ball->x;
    int ball_right = ball_left + ball->r / 2;

    if (ball_right <= left->x) {
        return RIGHT;
    }

    if (right->x + right->w <= ball_left) {
        return LEFT;
    }

    return NONE;
}

int main(int argc, char** argv) {
    char left_buff[256];
    char right_buff[256];

    Brick left_brick;
    Brick right_brick;
    float brick_speed;

    Ball ball;

    int scr_w;
    int scr_h;
    int new_scr_w;
    int new_scr_h;

    int frame_cnt;

    Winner winner = NONE;

    State game_state = IDLE;

    int left_score = 0;
    int right_score = 0;

    int right_str_len = 0;

    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(0, 0, "Pong. The Game");

    scr_w = GetScreenWidth();
    scr_h = GetScreenHeight();

    init_brick(&left_brick, scr_w, scr_h, true);
    init_brick(&right_brick, scr_w, scr_h, false);
    init_ball(&ball, scr_w, scr_h);

    SetTargetFPS(60);

    while (!WindowShouldClose()) {
        if (IsWindowResized()) {
            new_scr_w = GetScreenWidth();
            new_scr_h = GetScreenHeight();
            resize_brick(&left_brick, scr_w, scr_h, new_scr_w, new_scr_h);
            resize_brick(&right_brick, scr_w, scr_h, new_scr_w, new_scr_h);
            resize_ball(&ball, scr_w, scr_h, new_scr_w, new_scr_h);
            scr_w = new_scr_w;
            scr_h = new_scr_h;
            brick_speed = scr_h / 100.0f;
        }

        brick_speed = (float)scr_h * 0.01f;

        if (IsKeyDown(KEY_W))
            left_brick.y -= brick_speed;
        if (IsKeyDown(KEY_S))
            left_brick.y += brick_speed;
        if (IsKeyDown(KEY_UP))
            right_brick.y -= brick_speed;
        if (IsKeyDown(KEY_DOWN))
            right_brick.y += brick_speed;
        if (IsKeyDown(KEY_SPACE)) {
            if (game_state != START) {
                ball.vel_x = 0.005f * (frame_cnt & 1 ? 1 : -1);
                ball.vel_y = 0.005f * ((frame_cnt >> 2) & 1 ? 1 : -1);
                game_state = START;
                frame_cnt = 0;
            }
        }

        clip_brick(&left_brick, scr_h);
        clip_brick(&right_brick, scr_h);

        if (game_state == START) {
            if (120 < frame_cnt) {
                ball.vel_x += 0.0001 * (0 <= ball.vel_x ? 1 : -1);
                ball.vel_y += 0.0001 * (0 <= ball.vel_y ? 1 : -1);
                frame_cnt = 0;
            }
            update_ball(&ball, &left_brick, &right_brick, scr_w, scr_h);

            winner = check_winner(&ball, &left_brick, &right_brick);

            if (winner != NONE) {
                left_score += winner == LEFT;
                right_score += winner == RIGHT;
                game_state = FINISH;
                init_brick(&left_brick, scr_w, scr_h, true);
                init_brick(&right_brick, scr_w, scr_h, false);
                init_ball(&ball, scr_w, scr_h);
            }
        }

        BeginDrawing();

        ClearBackground(BLACK);

        DrawRectangle(left_brick.x, left_brick.y, left_brick.w, left_brick.h,
                      WHITE);

        DrawRectangle(right_brick.x, right_brick.y, right_brick.w,
                      right_brick.h, WHITE);

        if (game_state != FINISH) {
            DrawCircle(ball.x, ball.y, ball.r, WHITE);
        }

        sprintf(left_buff, "SCORE: %d", left_score);
        sprintf(right_buff, "SCORE: %d", right_score);
        right_str_len = strlen(right_buff);
        DrawText(left_buff, 0, 0, 20, WHITE);
        DrawText(right_buff, scr_w - (right_str_len / 2 + 1) * 20, 0, 20,
                 WHITE);

        if (winner == LEFT) {
            DrawText("LEFT PLAYER WINS", scr_w / 2 - 6 * 30 + 15,
                     scr_h / 2 - 30, 30, WHITE);
        }
        if (winner == RIGHT) {
            DrawText("RIGHT PLAYER WINS", scr_w / 2 - 6 * 30 + 15,
                     scr_h / 2 - 30, 30, WHITE);
        }

        EndDrawing();

        frame_cnt += 1;
    }

    CloseWindow();

    return 0;
}
