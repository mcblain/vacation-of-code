use sdl2::render;

const RAM_SIZE: usize = 0x8000;
const ROM_SIZE: usize = 0x8000;
const SCREEN_ADDR: usize = 0x4000;
const KEYBOARD_ADDR: usize = 0x6000;
const PIXEL_WIDTH: usize = 4;

type Word = i16;
type UWord = u16;

#[inline(always)]
fn to_signed(x: UWord) -> Word {
    unsafe { std::mem::transmute::<UWord, Word>(x) }
}

#[inline(always)]
fn to_unsigned(x: Word) -> UWord {
    unsafe { std::mem::transmute::<Word, UWord>(x) }
}

pub struct Hack {
    pub ram: Vec<Word>,
    rom: Vec<Word>,
    pc: usize,
    d_reg: Word, // data register
    a_reg: Word, // address register
}

pub fn new_hack() -> Hack {
    Hack {
        ram: vec![0; RAM_SIZE],
        rom: vec![0; ROM_SIZE],
        pc: 0,
        d_reg: 0,
        a_reg: 0,
    }
}

fn draw(addr: Word, val: Word, texture: &mut render::Texture) {
    let byte_addr = to_unsigned(addr) as usize - SCREEN_ADDR;
    let x = (byte_addr % 32) as usize;
    let y = byte_addr / 32 as usize;

    texture
        .with_lock(None, |pixels, pitch| {
            for i in 0..16 {
                let offset = y * pitch + (x * 16 + 15 - i) * PIXEL_WIDTH;
                let bright = (val >> i & 1) != 0;
                if bright {
                    pixels[offset] = 255;
                    pixels[offset + 1] = 0xda;
                    pixels[offset + 2] = 0x71;
                    pixels[offset + 3] = 0x7c;
                } else {
                    pixels[offset] = 255;
                    pixels[offset + 1] = 0xa2;
                    pixels[offset + 2] = 0x32;
                    pixels[offset + 3] = 0x3e;
                }
            }
        })
        .unwrap();
}

pub fn set_key_state(hack: &mut Hack, value: Word) {
    println!("{}", value);
    hack.ram[KEYBOARD_ADDR] = value;
}

pub fn load_instructions(hack: &mut Hack, instructions: &[Word]) {
    hack.rom[..instructions.len()].copy_from_slice(instructions);
}

pub fn tick(hack: &mut Hack, texture: &mut render::Texture) {
    let instruction = hack.rom[hack.pc];

    let is_a_instruction = instruction >> 15 == 0;

    if is_a_instruction {
        // A instruction
        hack.a_reg = instruction;
        hack.pc += 1;
        return;
    }

    // C instruction
    let jump = instruction & 0b111;
    let dest = (instruction >> 3) & 0b111;
    let comp = (instruction >> 6) & 0b111111;
    let a = (instruction >> 12) & 1;

    let a_reg = hack.a_reg;
    let d_reg = hack.d_reg;

    let comp_res = if a == 0 {
        match comp {
            0b101010 => 0,
            0b111111 => 1,
            0b111010 => -1,
            0b001100 => d_reg,
            0b110000 => a_reg,
            0b001101 => !d_reg,
            0b110001 => !a_reg,
            0b001111 => -d_reg,
            0b110011 => -a_reg,
            0b011111 => d_reg.wrapping_add(1),
            0b110111 => a_reg.wrapping_add(1),
            0b001110 => d_reg.wrapping_sub(1),
            0b110010 => a_reg.wrapping_sub(1),
            0b000010 => d_reg.wrapping_add(a_reg),
            0b010011 => d_reg.wrapping_sub(a_reg),
            0b000111 => a_reg.wrapping_sub(d_reg),
            0b000000 => d_reg & a_reg,
            0b010101 => d_reg | a_reg,
            _ => unreachable!(),
        }
    } else {
        let m_reg = hack.ram[hack.a_reg as usize];
        match comp {
            0b110000 => m_reg,
            0b110001 => !m_reg,
            0b110011 => -m_reg,
            0b110111 => m_reg.wrapping_add(1),
            0b110010 => m_reg.wrapping_sub(1),
            0b000010 => d_reg.wrapping_add(m_reg),
            0b010011 => d_reg.wrapping_sub(m_reg),
            0b000111 => m_reg.wrapping_sub(d_reg),
            0b000000 => d_reg & m_reg,
            0b010101 => d_reg | m_reg,
            _ => unreachable!(),
        }
    };

    if dest & 0b001 == 0b001 {
        let addr = a_reg as usize;
        if SCREEN_ADDR <= addr && addr < KEYBOARD_ADDR {
            draw(a_reg, comp_res, texture);
        } else if addr == KEYBOARD_ADDR {
            unimplemented!();
        } else {
            hack.ram[a_reg as usize] = comp_res;
        }
    }
    if dest & 0b010 == 0b010 {
        hack.d_reg = comp_res;
    }
    if dest & 0b100 == 0b100 {
        hack.a_reg = comp_res;
    }

    let can_we_jump = match jump {
        0b000 => false,
        0b001 => comp_res > 0,
        0b010 => comp_res == 0,
        0b011 => comp_res >= 0,
        0b100 => comp_res < 0,
        0b101 => comp_res != 0,
        0b110 => comp_res <= 0,
        0b111 => true,
        _ => unimplemented!(),
    };

    if can_we_jump {
        hack.pc = hack.a_reg as usize;
    } else {
        hack.pc += 1;
    }
}

pub fn disassemble(opcode: Word) -> String {
    let mut res = String::new();
    if ((opcode >> 15) & 1) == 1 {
        let comp = (opcode & 0x1fc0) >> 6;
        let dest = (opcode & 0x0038) >> 3;
        let jump = (opcode & 0b111) >> 0;
        let comp_str = match comp {
            0x2a => "0",
            0x3f => "1",
            0x3a => "-1",
            0x0c => "D",
            0x30 => "A",
            0x0d => "!D",
            0x31 => "!A",
            0x0f => "-D",
            0x33 => "-A",
            0x1f => "D+1",
            0x37 => "A+1",
            0x0e => "D-1",
            0x32 => "A-1",
            0x02 => "D+A",
            0x23 => "D-A",
            0x07 => "A-D",
            0x00 => "D&A",
            0x15 => "D|A",
            0x70 => "M",
            0x71 => "!M",
            0x73 => "-M",
            0x77 => "M+1",
            0x72 => "M-1",
            0x42 => "D+M",
            0x53 => "D-M",
            0x47 => "M-D",
            0x40 => "D&M",
            0x55 => "D|M",
            _ => "?",
        };
        let dest_str = match dest {
            0x00 => "",
            0x01 => "M",
            0x02 => "D",
            0x03 => "MD",
            0x04 => "A",
            0x05 => "AM",
            0x06 => "AD",
            0x07 => "AMD",
            _ => "?",
        };
        let jump_str = match jump {
            0x00 => "",
            0x01 => "JGT",
            0x02 => "JEQ",
            0x03 => "JGE",
            0x04 => "JLT",
            0x05 => "JNE",
            0x06 => "JLE",
            0x07 => "JMP",
            _ => "?",
        };

        res.push_str(dest_str);
        res.push_str("=");
        res.push_str(comp_str);
        res.push_str(";");
        res.push_str(jump_str);
    } else {
        res.push_str("@");
        res.push_str(&(opcode & 0x7fff).to_string());
    }

    res
}
