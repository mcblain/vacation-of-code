mod core;
use std::{env::args, fs};

use sdl2::{self, event::Event, keyboard::Keycode, pixels::PixelFormatEnum};
use std::collections::HashMap;

const SCREEN_WIDTH: u32 = 512;
const SCREEN_HEIGHT: u32 = 256;

fn get_keycodes() -> HashMap<Keycode, i16> {
    HashMap::from([
        (Keycode::Space, 32),
        (Keycode::Exclaim, 33),
        (Keycode::Quotedbl, 34),
        (Keycode::Hash, 35),
        (Keycode::Dollar, 36),
        (Keycode::Percent, 37),
        (Keycode::Ampersand, 38),
        (Keycode::Comma, 39),
        (Keycode::LeftParen, 40),
        (Keycode::RightParen, 41),
        (Keycode::Asterisk, 42),
        (Keycode::Plus, 43),
        (Keycode::Comma, 44),
        (Keycode::Minus, 45),
        (Keycode::Period, 46),
        (Keycode::Slash, 47),
        (Keycode::Num0, 48),
        (Keycode::Num1, 49),
        (Keycode::Num2, 50),
        (Keycode::Num3, 51),
        (Keycode::Num4, 52),
        (Keycode::Num5, 53),
        (Keycode::Num6, 54),
        (Keycode::Num7, 55),
        (Keycode::Num8, 56),
        (Keycode::Num9, 57),
        (Keycode::Colon, 58),
        (Keycode::Semicolon, 59),
        (Keycode::Less, 60),
        (Keycode::Equals, 61),
        (Keycode::Greater, 62),
        (Keycode::Question, 63),
        (Keycode::At, 64),
    ])
}

fn parse_opcodes(source: &str) -> Vec<i16> {
    let offset = '0' as i16;
    let mut opcodes: Vec<i16> = Vec::with_capacity(source.lines().count());

    for line in source.lines() {
        let mut opcode: i16 = 0;

        for (i, ch) in line.chars().enumerate() {
            opcode |= ((ch as i16 - offset) & 1) << (15 - i);
        }

        opcodes.push(opcode);
    }

    opcodes
}

fn main() {
    let args: Vec<String> = args().skip(1).collect();

    if args.len() != 1 {
        return;
    }

    let source = fs::read_to_string(&args[0]).expect("Wrong file path");
    let code = parse_opcodes(&source);

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("Hack VM", SCREEN_WIDTH, SCREEN_HEIGHT)
        // .resizable()
        .build()
        .unwrap();

    let mut canvas = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .unwrap();
    let texture_creator = canvas.texture_creator();

    let mut texture = texture_creator
        .create_texture_streaming(Some(PixelFormatEnum::RGBA8888), SCREEN_WIDTH, SCREEN_HEIGHT)
        .unwrap();

    texture
        .with_lock(None, |pixels, _| {
            for i in 0..(pixels.len() / 4) {
                let offset = i * 4;
                pixels[offset] = 255;
                pixels[offset + 1] = 0xa2;
                pixels[offset + 2] = 0x32;
                pixels[offset + 3] = 0x3e;
            }
        })
        .unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();

    let keycodes = get_keycodes();
    let mut hack = core::new_hack();

    // hack.ram[0] = 100;

    core::load_instructions(&mut hack, &code);

    'running: loop {
        if let Some(event) = &event_pump.poll_event() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::Window {
                    // win_event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    // resize_texture(ctx, w as u32, h as u32);
                    // update_window(ctx);
                }
                _ => {}
            }
        }

        for _ in 0..128 {
            core::tick(&mut hack, &mut texture);
        }

        let keyboard_state = event_pump.keyboard_state();

        for keycode in keyboard_state
            .pressed_scancodes()
            .filter_map(Keycode::from_scancode)
        {
            if let Some(key) = keycodes.get(&keycode) {
                core::set_key_state(&mut hack, *key);
            }
        }

        canvas.clear();
        canvas.copy(&texture, None, None).unwrap();
        canvas.present();
    }
}
