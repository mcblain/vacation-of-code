use std::{iter::Peekable, str::Chars};

pub struct Scanner<'a> {
    curr: char,
    lexeme: String,
    line: usize,
    source: Peekable<Chars<'a>>,
    finished: bool,
}

const EOF: char = '\0';

fn is_digit(c: char) -> bool {
    ('0'..='9').contains(&c)
}

fn is_alpha(c: char) -> bool {
    ('a'..='z').contains(&c) || ('A'..='Z').contains(&c) || c == '_'
}

impl<'a> Scanner<'a> {
    fn advance(&mut self) -> char {
        let prev = self.curr;
        self.lexeme.push(prev);
        match self.source.next() {
            Some(c) => {
                self.curr = c;
                prev
            }
            None => {
                self.curr = EOF;
                EOF
            }
        }
    }

    fn check_keyword(&self, start: usize, rest: &str, kind: TokenKind) -> TokenKind {
        let lexeme_len = self.lexeme.len();
        let rest_len = rest.len();

        if lexeme_len < start + rest_len {
            return TokenKind::Identifier;
        }

        if rest == &self.lexeme[start..(start + rest_len)] {
            return kind;
        }

        TokenKind::Identifier
    }

    fn expect(&mut self, expected: char) -> bool {
        if self.peek() == expected {
            self.advance();
            true
        } else {
            false
        }
    }

    fn identifier(&mut self) -> Token {
        while is_alpha(self.peek()) || is_digit(self.peek()) {
            self.advance();
        }

        self.make_token(self.identifier_kind())
    }

    fn identifier_kind(&self) -> TokenKind {
        if let Some(c) = self.lexeme.chars().next() {
            return match c {
                'a' => self.check_keyword(1, "nd", TokenKind::And),
                'c' => self.check_keyword(1, "lass", TokenKind::Class),
                'e' => self.check_keyword(1, "lse", TokenKind::Else),
                'f' => {
                    let mut chars = self.lexeme.chars();
                    if 2 < self.lexeme.len() {
                        return match chars.nth(1).unwrap() {
                            'a' => self.check_keyword(2, "lse", TokenKind::False),
                            'o' => self.check_keyword(2, "r", TokenKind::For),
                            'u' => self.check_keyword(2, "n", TokenKind::Fun),
                            _ => TokenKind::Identifier,
                        };
                    }
                    TokenKind::Identifier
                }
                'i' => self.check_keyword(1, "f", TokenKind::If),
                'n' => self.check_keyword(1, "il", TokenKind::Nil),
                'o' => self.check_keyword(1, "r", TokenKind::Or),
                'p' => self.check_keyword(1, "rint", TokenKind::Print),
                'r' => self.check_keyword(1, "eturn", TokenKind::Return),
                's' => self.check_keyword(1, "uper", TokenKind::Super),
                't' => {
                    let mut chars = self.lexeme.chars();
                    if 2 < self.lexeme.len() {
                        return match chars.nth(1).unwrap() {
                            'h' => self.check_keyword(2, "is", TokenKind::This),
                            'r' => self.check_keyword(2, "ue", TokenKind::True),
                            _ => TokenKind::Identifier,
                        };
                    }
                    TokenKind::Identifier
                }
                'v' => self.check_keyword(1, "ar", TokenKind::Var),
                'w' => self.check_keyword(1, "hile", TokenKind::While),
                _ => TokenKind::Identifier,
            };
        };

        TokenKind::Identifier
    }

    fn is_at_end(&mut self) -> bool {
        self.curr == EOF
    }

    fn make_error_token(&mut self, message: String) -> Token {
        self.lexeme.clear();
        Token::new(TokenKind::Error, message, self.line)
    }

    fn make_token(&mut self, kind: TokenKind) -> Token {
        Token::new(kind, std::mem::take(&mut self.lexeme), self.line)
    }

    pub fn new(source: &'a str) -> Self {
        let mut peekable = source.chars().peekable();
        let curr = match peekable.next() {
            Some(c) => c,
            None => EOF,
        };

        Self {
            source: peekable,
            lexeme: String::new(),
            curr,
            line: 1,
            finished: false,
        }
    }

    fn number(&mut self) -> Token {
        while is_digit(self.peek()) {
            self.advance();
        }

        if self.peek() == '.' && is_digit(self.peek_next()) {
            self.advance();

            while is_digit(self.peek()) {
                self.advance();
            }
        };

        self.make_token(TokenKind::Number)
    }

    fn peek(&mut self) -> char {
        self.curr
    }

    fn peek_next(&mut self) -> char {
        match self.source.peek() {
            Some(c) => *c,
            None => EOF,
        }
    }

    fn skip(&mut self) {
        match self.source.next() {
            Some(c) => self.curr = c,
            None => self.curr = EOF,
        };
    }

    pub fn skip_whitespace(&mut self) {
        let mut c = self.peek();

        while c != EOF {
            if c == ' ' || c == '\r' || c == '\t' {
                self.skip();
            } else if c == '\n' {
                self.line += 1;
                self.skip();
            } else if c == '/' && self.peek_next() == '/' {
                while self.peek() != '\n' && !self.is_at_end() {
                    self.skip();
                }
            } else {
                break;
            }

            c = self.peek();
        }
    }

    fn string(&mut self) -> Token {
        while self.peek() != '"' && !self.is_at_end() {
            if self.peek() == '\n' {
                self.line += 1;
            }
            self.advance();
        }

        if self.is_at_end() {
            return self.make_error_token("Unterminated string.".to_string());
        }

        self.advance();

        self.make_token(TokenKind::String)
    }
}

impl<'a> Iterator for Scanner<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        self.skip_whitespace();

        let c = self.advance();

        if self.finished {
            return None;
        }

        if self.is_at_end() {
            self.finished = true;
            return Some(self.make_token(TokenKind::Eof));
        }

        if is_alpha(c) {
            return Some(self.identifier());
        }

        if is_digit(c) {
            return Some(self.number());
        }

        let token = match c {
            '(' => self.make_token(TokenKind::LeftParen),
            ')' => self.make_token(TokenKind::RightParen),
            '{' => self.make_token(TokenKind::LeftBrace),
            '}' => self.make_token(TokenKind::RightBrace),
            ';' => self.make_token(TokenKind::Semicolon),
            ',' => self.make_token(TokenKind::Comma),
            '.' => self.make_token(TokenKind::Dot),
            '-' => self.make_token(TokenKind::Minus),
            '+' => self.make_token(TokenKind::Plus),
            '/' => self.make_token(TokenKind::Slash),
            '*' => self.make_token(TokenKind::Star),
            '!' => {
                let kind = if self.expect('=') {
                    TokenKind::BangEqual
                } else {
                    TokenKind::Bang
                };

                self.make_token(kind)
            }
            '=' => {
                let kind = if self.expect('=') {
                    TokenKind::EqualEqual
                } else {
                    TokenKind::Equal
                };
                self.make_token(kind)
            }
            '<' => {
                let kind = if self.expect('=') {
                    TokenKind::LessEqual
                } else {
                    TokenKind::Less
                };
                self.make_token(kind)
            }
            '>' => {
                let kind = if self.expect('=') {
                    TokenKind::GreaterEqual
                } else {
                    TokenKind::Greater
                };
                self.make_token(kind)
            }
            '"' => self.string(),
            _ => self.make_error_token(format!("Unexpected character. \"{}\"", self.lexeme)),
        };

        Some(token)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum TokenKind {
    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,
    Comma,
    Dot,
    Minus,
    Plus,
    Semicolon,
    Slash,
    Star,
    Bang,
    BangEqual,
    Equal,
    EqualEqual,
    Greater,
    GreaterEqual,
    Less,
    LessEqual,
    Identifier,
    String,
    Number,
    And,
    Class,
    Else,
    False,
    For,
    Fun,
    If,
    Nil,
    Or,
    Print,
    Return,
    Super,
    This,
    True,
    Var,
    While,
    Error,
    Eof,
}

#[derive(Clone, Debug)]
pub struct Token {
    pub kind: TokenKind,
    pub lexeme: String,
    pub line: usize,
}

impl Token {
    pub fn new(kind: TokenKind, lexeme: String, line: usize) -> Self {
        Self { kind, lexeme, line }
    }
}
