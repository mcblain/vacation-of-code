use crate::chunk::{Chunk, OpCode};

fn constant_instruction(name: &str, chunk: &Chunk, offset: usize) -> usize {
    let constant = chunk.code[offset + 1];
    print!("{:-16} {:4} '", name, constant);
    chunk.constants[constant as usize].print();
    println!("'");
    offset + 2
}

fn simple_instruction(name: &str, offset: usize) -> usize {
    println!("{}", name);
    offset + 1
}

pub fn disassemble_instruction(chunk: &Chunk, offset: usize) -> usize {
    print!("{:04} ", offset);

    let lines = &chunk.lines;

    if 0 < offset && lines[offset] == lines[offset - 1] {
        print!("   | ");
    } else {
        print!("{:4} ", lines[offset]);
    }

    let instruction: u8 = chunk.code[offset];

    match OpCode::from(instruction) {
        OpCode::Not => simple_instruction("Not", offset),
        OpCode::Nil => simple_instruction("Nil", offset),
        OpCode::True => simple_instruction("True", offset),
        OpCode::False => simple_instruction("False", offset),
        OpCode::Equal => simple_instruction("Equal", offset),
        OpCode::Greater => simple_instruction("Greater", offset),
        OpCode::Less => simple_instruction("Less", offset),
        OpCode::Return => simple_instruction("Return", offset),
        OpCode::Add => simple_instruction("Add", offset),
        OpCode::Subtract => simple_instruction("Subtract", offset),
        OpCode::Multiply => simple_instruction("Multiply", offset),
        OpCode::Divide => simple_instruction("Divide", offset),
        OpCode::Negate => simple_instruction("Negate", offset),
        OpCode::Constant => constant_instruction("Constant", chunk, offset),
    }
}

pub fn disassemble_chunk(chunk: &Chunk, name: &str) {
    println!("== {} ==", name);

    let mut offset: usize = 0;

    while offset < chunk.code.len() {
        offset = disassemble_instruction(chunk, offset);
    }
}
