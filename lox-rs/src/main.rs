mod chunk;
mod compiler;
mod debug;
mod scanner;
mod value;
mod vm;
use std::io::Write;
use std::process;
use vm::{InterpretResult, VM};

fn main() {
    let mut vm = VM::new();

    let argv: Vec<String> = std::env::args().collect();
    let argc = argv.len();

    if argc == 1 {
        repl(&mut vm);
    } else if argc == 2 {
        run_file(&mut vm, &argv[1]);
    } else {
        eprintln!("Usage: clox [path]\n");
        std::process::exit(64);
    }
}

pub fn read_file(path: &str) -> String {
    match std::fs::read_to_string(path) {
        Ok(str) => str,
        Err(_) => {
            eprintln!("Could not open file {}", path);
            process::exit(74);
        }
    }
}

pub fn run_file(vm: &mut VM, path: &str) {
    let source = read_file(path);
    let result: InterpretResult = vm.interpret(&source);

    match result {
        InterpretResult::CompileErr => process::exit(65),
        InterpretResult::RuntimeErr => process::exit(70),
        _ => {}
    }
}

pub fn repl(vm: &mut VM) {
    let mut line_buff = String::with_capacity(120);
    let stdin = std::io::stdin();
    let stdout = std::io::stdout();

    loop {
        print!("> ");
        stdout.lock().flush().unwrap();
        // line_buff.clear();

        if let Ok(len) = stdin.read_line(&mut line_buff) {
            if len == 0 {
                println!();
                break;
            }

            vm.interpret(&line_buff);
        }

        line_buff.clear();
    }
}
