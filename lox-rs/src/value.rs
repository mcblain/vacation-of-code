use std::rc::Rc;

#[derive(Clone)]
pub enum Value {
    Bool(bool),
    Nil,
    Number(f32),
    Obj(ObjRef),
}

pub type ObjRef = Rc<ObjValue>;

#[derive(Clone)]
pub enum ObjValue {
    String(String),
}

impl ObjValue {
    fn print(&self) {
        match self {
            ObjValue::String(str) => print!("{}", str),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        use Value::*;
        match (self, other) {
            (Bool(a), Bool(b)) => *a && *b,
            (Number(a), Number(b)) => *a == *b,
            (Obj(obj_a), Obj(obj_b)) => match (&**obj_a, &**obj_b) {
                (ObjValue::String(a), ObjValue::String(b)) => a == b,
                // _ => false,
            },
            (Nil, Nil) => true,
            _ => false,
        }
    }
}

impl From<f32> for Value {
    fn from(num: f32) -> Self {
        Value::Number(num)
    }
}

impl From<Value> for f32 {
    fn from(val: Value) -> Self {
        match val {
            Value::Number(num) => num,
            _ => panic!("Cast non-number value to f32"),
        }
    }
}

impl Value {
    pub fn is_falsey(&self) -> bool {
        match self {
            Self::Nil => true,
            Self::Bool(val) => !val,
            _ => false,
        }
    }

    pub fn print(&self) {
        match self {
            Self::Number(num) => {
                print!("{}", num);
            }
            Self::Bool(val) => {
                if *val {
                    print!("true")
                } else {
                    print!("false")
                }
            }
            Self::Nil => print!("Nil"),
            Self::Obj(obj) => obj.print(),
        }
    }
}
