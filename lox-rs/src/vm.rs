use std::rc::Rc;

use crate::{
    chunk::{Chunk, OpCode},
    compiler::Compiler,
    debug,
    value::{ObjValue, Value},
};

const STACK_SIZE: usize = 256;

pub enum InterpretResult {
    Ok,
    CompileErr,
    RuntimeErr,
}

pub struct Stack(Vec<Value>);

impl Stack {
    pub fn new() -> Self {
        Self(Vec::with_capacity(STACK_SIZE))
    }

    pub fn peek(&self, dist: usize) -> &Value {
        &self.0[self.0.len() - dist]
    }

    pub fn pop(&mut self) -> Value {
        self.0.pop().unwrap()
    }

    pub fn push(&mut self, value: Value) {
        self.0.push(value);
    }

    pub fn reset(&mut self) {
        self.0.clear();
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

impl std::ops::Index<usize> for Stack {
    type Output = Value;
    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

pub struct VM {
    chunk: Chunk,
    ip: usize,
    stack: Stack,
}

impl VM {
    fn get_bin_op_args(&mut self) -> Option<(f32, f32)> {
        match (self.stack.pop(), self.stack.pop()) {
            (Value::Number(b), Value::Number(a)) => Some((a, b)),
            _ => {
                self.runtime_error("Operands must be numbers");
                None
            }
        }
    }

    pub fn interpret(&mut self, source: &str) -> InterpretResult {
        let mut compiler = Compiler::new(source, &mut self.chunk);

        if !compiler.compile() {
            return InterpretResult::CompileErr;
        };

        self.run()
    }

    pub fn new() -> Self {
        Self {
            chunk: Chunk::new(),
            ip: 0,
            stack: Stack::new(),
        }
    }

    #[inline(always)]
    fn read_byte(&mut self) -> u8 {
        self.ip += 1;
        self.chunk.code[self.ip - 1]
    }

    fn read_constant(&mut self) -> Value {
        let addr = self.read_byte() as usize;
        self.chunk.constants[addr].clone()
    }

    fn runtime_error(&mut self, msg: impl AsRef<str>) {
        eprintln!("{}", msg.as_ref());
        let line = self.chunk.lines[self.ip - 1];
        eprintln!("[line {}] in script", line);
        self.stack.reset();
    }

    pub fn run(&mut self) -> InterpretResult {
        if self.chunk.code.is_empty() {
            return InterpretResult::CompileErr;
        }

        loop {
            #[cfg(debug_assertions)]
            {
                debug::disassemble_instruction(&self.chunk, self.ip);
                print!("          ");
                for i in 0..(self.stack.len()) {
                    print!("[");
                    self.stack[i].print();
                    print!("]");
                }
                println!();
            }

            match OpCode::from(self.read_byte()) {
                OpCode::Constant => {
                    let constant = self.read_constant();
                    self.stack.push(constant);
                }
                OpCode::Nil => self.stack.push(Value::Nil),
                OpCode::True => self.stack.push(Value::Bool(true)),
                OpCode::False => self.stack.push(Value::Bool(false)),
                OpCode::Equal => {
                    let b = self.stack.pop();
                    let a = self.stack.pop();
                    self.stack.push(Value::Bool(a == b));
                }
                OpCode::Greater => match self.get_bin_op_args() {
                    Some((a, b)) => self.stack.push(Value::Bool(a > b)),
                    None => return InterpretResult::RuntimeErr,
                },
                OpCode::Less => match self.get_bin_op_args() {
                    Some((a, b)) => self.stack.push(Value::Bool(a < b)),
                    None => return InterpretResult::RuntimeErr,
                },
                OpCode::Add => match (self.stack.pop(), self.stack.pop()) {
                    (Value::Number(b), Value::Number(a)) => self.stack.push((a + b).into()),
                    (Value::Obj(obj_b), Value::Obj(obj_a)) => match (&*obj_a, &*obj_b) {
                        (ObjValue::String(a), ObjValue::String(b)) => {
                            let mut str = a.clone();
                            str.push_str(&b);
                            self.stack.push(Value::Obj(Rc::new(ObjValue::String(str))));
                        }
                    },
                    _ => {
                        self.runtime_error("Operands must be numbers, or two strings");
                    }
                },
                OpCode::Subtract => match self.get_bin_op_args() {
                    Some((a, b)) => self.stack.push((a - b).into()),
                    None => return InterpretResult::RuntimeErr,
                },
                OpCode::Multiply => match self.get_bin_op_args() {
                    Some((a, b)) => self.stack.push((a * b).into()),
                    None => return InterpretResult::RuntimeErr,
                },
                OpCode::Divide => match self.get_bin_op_args() {
                    Some((a, b)) => self.stack.push((a / b).into()),
                    None => return InterpretResult::RuntimeErr,
                },
                OpCode::Not => {
                    let arg = self.stack.pop();
                    self.stack.push(Value::Bool(arg.is_falsey()))
                }
                OpCode::Negate => match self.stack.pop() {
                    Value::Number(num) => self.stack.push(Value::Number(-num)),
                    x => {
                        self.stack.push(x);
                        self.runtime_error("Operand must be a number");
                        return InterpretResult::RuntimeErr;
                    }
                },
                OpCode::Return => {
                    self.stack.pop().print();
                    println!();
                    return InterpretResult::Ok;
                }
            }
        }
    }
}

impl Default for VM {
    fn default() -> Self {
        Self::new()
    }
}
