use crate::value::Value;

#[derive(Clone, Copy)]
pub enum OpCode {
    Constant,
    Nil,
    True,
    False,
    Equal,
    Greater,
    Less,
    Add,
    Subtract,
    Multiply,
    Divide,
    Not,
    Negate,
    Return,
}

impl From<u8> for OpCode {
    fn from(code: u8) -> Self {
        unsafe { std::mem::transmute::<u8, Self>(code) }
    }
}

impl From<OpCode> for u8 {
    fn from(code: OpCode) -> Self {
        code as Self
    }
}

pub struct Chunk {
    pub code: Vec<u8>,
    pub constants: Vec<Value>,
    pub lines: Vec<usize>,
}

impl Chunk {
    pub fn new() -> Chunk {
        Chunk {
            code: Vec::new(),
            constants: Vec::new(),
            lines: Vec::new(),
        }
    }

    pub fn add_constant(&mut self, value: Value) -> (u8, bool) {
        self.constants.push(value);
        let len = self.constants.len() - 1;
        let overflow = (u8::MAX as usize) < len;
        (len as u8, overflow)
    }

    pub fn write(&mut self, op_code: u8, line: usize) {
        self.code.push(op_code);
        self.lines.push(line);
    }
}
