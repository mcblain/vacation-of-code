use std::{mem, rc::Rc};

use crate::{
    chunk::{Chunk, OpCode},
    debug,
    scanner::{Scanner, Token, TokenKind},
    value::{ObjValue, Value},
};

macro_rules! rule_item {
    ($arr:ident; $( $k:ident => $prefix:expr, $infix:expr, $precendence:ident; )* ) => {

        $($arr[TokenKind::$k as usize] = ParseRule {
            prefix: $prefix,
            infix: $infix,
            precendence: Precendence::$precendence,
        };)*
    };
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Precendence {
    None,
    Assignment,
    Or,
    And,
    Equality,
    Comparison,
    Term,
    Factor,
    Unary,
    Call,
    Primary,
}

impl From<u8> for Precendence {
    fn from(p: u8) -> Self {
        unsafe { std::mem::transmute::<u8, Self>(p) }
    }
}

type ParseFn<'a> = fn(&mut Compiler<'a>) -> ();

#[derive(Clone, Copy)]
struct ParseRule<'a> {
    prefix: Option<ParseFn<'a>>,
    infix: Option<ParseFn<'a>>,
    precendence: Precendence,
}

pub struct Compiler<'a> {
    compiling_chunk: &'a mut Chunk,
    scanner: Scanner<'a>,
    prev: Token,
    curr: Token,
    panic_mode: bool,
    had_error: bool,
}

impl<'a> Compiler<'a> {
    const PARSE_RULES: [ParseRule<'a>; 40] = {
        let mut arr: [ParseRule<'a>; 40] = [ParseRule {
            prefix: None,
            infix: None,
            precendence: Precendence::None,
        }; 40];

        rule_item![
            arr;
            LeftParen    => Some(Compiler::grouping), None,                   None;
            RightParen   => None,                     None,                   None;
            LeftBrace    => None,                     None,                   None;
            RightBrace   => None,                     None,                   None;
            Comma        => None,                     None,                   None;
            Dot          => None,                     None,                   None;
            Minus        => Some(Compiler::unary),    Some(Compiler::binary), Term;
            Plus         => None,                     Some(Compiler::binary), Term;
            Semicolon    => None,                     None,                   None;
            Slash        => None,                     Some(Compiler::binary), Factor;
            Star         => None,                     Some(Compiler::binary), Factor;
            Bang         => Some(Compiler::unary),    None,                   None;
            BangEqual    => None,                     Some(Compiler::binary), Equality;
            Equal        => None,                     None,                   Comparison;
            EqualEqual   => None,                     Some(Compiler::binary), Comparison;
            Greater      => None,                     Some(Compiler::binary), Comparison;
            GreaterEqual => None,                     Some(Compiler::binary), Comparison;
            Less         => None,                     Some(Compiler::binary), Comparison;
            LessEqual    => None,                     Some(Compiler::binary), Comparison;
            Identifier   => None,                     None,                   None;
            String       => Some(Compiler::string),   None,                   None;
            Number       => Some(Compiler::number),   None,                   None;
            And          => None,                     None,                   None;
            Class        => None,                     None,                   None;
            Else         => None,                     None,                   None;
            False        => Some(Compiler::literal),  None,                   None;
            For          => None,                     None,                   None;
            Fun          => None,                     None,                   None;
            If           => None,                     None,                   None;
            Nil          => Some(Compiler::literal),  None,                   None;
            Or           => None,                     None,                   None;
            Print        => None,                     None,                   None;
            Return       => None,                     None,                   None;
            Super        => None,                     None,                   None;
            This         => None,                     None,                   None;
            True         =>  Some(Compiler::literal), None,                   None;
            Var          => None,                     None,                   None;
            While        => None,                     None,                   None;
            Error        => None,                     None,                   None;
            Eof          => None,                     None,                   None;
        ];

        arr
    };

    pub fn advance(&mut self) {
        mem::swap(&mut self.prev, &mut self.curr);

        while let Some(token) = self.scanner.next() {
            self.curr = token;
            if self.curr.kind != TokenKind::Error {
                return;
            }

            self.error_at_curr(&self.curr.lexeme);
            self.set_error();
        }
    }

    fn binary(&mut self) {
        let op_kind = self.prev.kind;
        let rule = self.get_rule(op_kind);
        self.parse_precedence(Precendence::from(rule.precendence as u8 + 1));

        match op_kind {
            TokenKind::Plus => self.emit_byte(OpCode::Add),
            TokenKind::Minus => self.emit_byte(OpCode::Subtract),
            TokenKind::Star => self.emit_byte(OpCode::Multiply),
            TokenKind::Slash => self.emit_byte(OpCode::Divide),
            TokenKind::BangEqual => self.emit_bytes(OpCode::Equal, OpCode::Not),
            TokenKind::EqualEqual => self.emit_byte(OpCode::Equal),
            TokenKind::Greater => self.emit_byte(OpCode::Greater),
            TokenKind::GreaterEqual => self.emit_bytes(OpCode::Less, OpCode::Not),
            TokenKind::Less => self.emit_byte(OpCode::Less),
            TokenKind::LessEqual => self.emit_bytes(OpCode::Greater, OpCode::Not),
            _ => unreachable!(),
        }
    }

    pub fn compile(&mut self) -> bool {
        self.advance();
        self.expression();
        self.consume(TokenKind::Eof, "Expect end of expression");
        self.end_compiler();

        !self.had_error
    }

    fn consume(&mut self, kind: TokenKind, msg: &str) {
        if self.curr.kind == kind {
            self.advance();
            return;
        }

        self.error_at_curr(msg);
        self.set_error();
    }

    fn emit_byte(&mut self, byte: impl Into<u8>) {
        self.compiling_chunk.write(byte.into(), self.prev.line);
    }

    fn emit_bytes<T: Into<u8>>(&mut self, byte_a: T, byte_b: T) {
        self.emit_byte(byte_a.into());
        self.emit_byte(byte_b.into());
    }

    fn emit_constant(&mut self, value: Value) {
        let addr = self.make_constant(value);
        self.emit_bytes(OpCode::Constant.into(), addr);
    }

    fn emit_return(&mut self) {
        self.emit_byte(OpCode::Return);
    }

    fn end_compiler(&mut self) {
        self.emit_return();

        #[cfg(debug_assertions)]
        {
            if !self.had_error {
                debug::disassemble_chunk(self.compiling_chunk, "code");
            }
        }
    }

    fn error_at(&self, token: &Token, msg: &str) {
        if self.panic_mode {
            return;
        }

        eprint!("[line {}] Error", token.line);

        match token.kind {
            TokenKind::Error => {}
            _ => {
                eprint!(" at '{}'", token.lexeme);
            }
        }

        eprintln!(": {}", msg);
    }

    fn error_at_curr(&self, msg: &str) {
        self.error_at(&self.curr, msg);
    }

    fn error_at_prev(&self, msg: &str) {
        self.error_at(&self.prev, msg);
    }

    fn expression(&mut self) {
        self.parse_precedence(Precendence::Assignment);
    }

    fn get_rule(&self, kind: TokenKind) -> &ParseRule<'a> {
        &Compiler::PARSE_RULES[kind as usize]
    }

    fn grouping(&mut self) {
        self.expression();
        self.consume(TokenKind::RightParen, "Expect ')' after expression.");
    }

    fn make_constant(&mut self, value: Value) -> u8 {
        let (addr, overflow) = self.compiling_chunk.add_constant(value);

        if overflow {
            self.error_at_prev("Too many constants in one chunk");
            return 0;
        }

        addr
    }

    pub fn new(source: &'a str, chunk: &'a mut Chunk) -> Self {
        let dummy_token = Token::new(TokenKind::Eof, String::new(), 0);
        Self {
            compiling_chunk: chunk,
            scanner: Scanner::new(source),
            prev: dummy_token.clone(),
            curr: dummy_token,
            panic_mode: false,
            had_error: false,
        }
    }

    fn string(&mut self) {
        let str = self.prev.lexeme[1..(self.prev.lexeme.len() - 1)].to_owned();
        self.emit_constant(Value::Obj(Rc::new(ObjValue::String(str))))
    }

    fn literal(&mut self) {
        match self.prev.kind {
            TokenKind::True => self.emit_byte(OpCode::True),
            TokenKind::False => self.emit_byte(OpCode::False),
            TokenKind::Nil => self.emit_byte(OpCode::Nil),
            _ => {}
        };
    }

    fn number(&mut self) {
        let value: f32 = self
            .prev
            .lexeme
            .parse()
            .expect("Failed parsing number token.");

        self.emit_constant(Value::Number(value));
    }

    fn parse_precedence(&mut self, precendence: Precendence) {
        self.advance();
        let prefix_rule = self.get_rule(self.prev.kind).prefix;

        match prefix_rule {
            Some(rule_fn) => rule_fn(self),
            None => {
                self.error_at_prev("Expect expression.");
                return;
            }
        }

        while precendence <= self.get_rule(self.curr.kind).precendence {
            self.advance();
            if let Some(infix_rule) = self.get_rule(self.prev.kind).infix {
                infix_rule(self);
            }
        }
    }

    fn set_error(&mut self) {
        self.had_error = true;
        self.panic_mode = true;
    }

    fn unary(&mut self) {
        let kind = self.prev.kind;

        self.parse_precedence(Precendence::Unary);

        match kind {
            TokenKind::Minus => self.emit_byte(OpCode::Negate),
            TokenKind::Bang => self.emit_byte(OpCode::Not),
            _ => unreachable!(),
        }
    }
}
