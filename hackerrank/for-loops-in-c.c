#include <stdio.h>

static char* nums[] = {
    "one", "two", "three", "four", "five",
    "six", "seven", "eight", "nine"
};

static char* odds[] = {
    "even", "odd"
};


int main() 
{
    int a, b;
    int i;
    scanf("%d\n%d", &a, &b);
    
    for (i = a; i <= b; i += 1) {
        if (i <= 9) {
           printf("%s\n", nums[i-1]); 
        } else {
           printf("%s\n", odds[i & 1]); 
        }
    }

    return 0;
}

