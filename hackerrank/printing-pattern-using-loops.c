#include <stdio.h>

#define MAX(a, b) ((a) < (b) ? (b) : (a))

void line(int n, int j) {
    int i, x, y;
    x = n - j;
    
    for (i = 0; i < n; i += 1) {
        y = n - i;
        printf("%d ", MAX(x, y));
    }
    for (i = n-2; 0 <= i; i -= 1) {
        y = n - i;
        printf("%d ", MAX(x, y));
    }
}

int main() 
{
    int n, i;
    
    scanf("%d", &n);
      
    for (i = 0; i < n; i += 1) {
        line(n, i);
        putchar('\n');
    }
    for (i = n-2; 0 <= i; i -= 1) {
        line(n, i);
        putchar('\n');
    }
      
    return 0;
}