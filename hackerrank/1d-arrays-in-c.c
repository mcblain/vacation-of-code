#include <stdio.h>
#include <stdlib.h>

int main() {
    int n, i;
    int* nums;
    int sum = 0;
    
    scanf("%d", &n); 
    
    nums = malloc(sizeof(int) * n);
    
    for (i = 0; i < n; i += 1) {
        scanf("%d", &nums[i]);
    }
    
    for (i = 0; i < n; i += 1) {
        sum += nums[i]; 
    }
    
    printf("%d", sum);

    return 0;
}
