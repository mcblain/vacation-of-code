#include <stdio.h>

#define MAX(val, max, k) ((val < k) && (max < val) ? val : max)

void calculate_the_maximum(int n, int k) {
    int a, b;
    int x, y, z;
    
    int max_and = 0;
    int max_or = 0;
    int max_xor = 0;
    
    for (a = 1; a <= n; a += 1) {
        for (b = a + 1; b <= n; b += 1) {
            x = a & b;
            max_and = MAX(x, max_and, k); 
            y = a | b;
            max_or = MAX(y, max_or, k);
            z = a ^ b;
            max_xor = MAX(z, max_xor, k);
        }
    }
    
    printf("%d\n%d\n%d\n", max_and, max_or, max_xor);
}

int main() {
    int n, k;
  
    scanf("%d %d", &n, &k);
    calculate_the_maximum(n, k);
 
    return 0;
}
