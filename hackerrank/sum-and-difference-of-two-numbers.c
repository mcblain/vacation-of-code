#include <stdio.h>

int main() {
    int a_int, b_int;
    float a_float, b_float;
    
    int sum_int, dif_int; 
    float sum_float, dif_float;
    
    scanf("%d%d%f%f", &a_int, &b_int, &a_float, &b_float); 
    
    sum_int = a_int + b_int;
    dif_int = a_int - b_int;
    
    sum_float = a_float + b_float;
    dif_float = a_float - b_float; 
    
    printf("%d %d\n%.1f %.1f", sum_int, dif_int, sum_float, dif_float);
    
    return 0;
}
