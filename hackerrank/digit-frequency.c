#include <stdio.h>
#include <stdbool.h>

bool is_digit(char c) {
    return '0' <= c && c <= '9';
}

int main() {
    int i;
    int x;
    char str[1000];
    int freq[10];
    
    scanf("%s", str);
    
    for (i = 0; i < 10; i += 1) {
        freq[i] = 0;
    } 
    
    for (i = 0; str[i] != '\0'; i += 1) {
       if (is_digit(str[i])) {
           freq[str[i]-'0'] += 1;
       } 
    }
    
    for (i = 0; i < 10; i += 1) {
        printf("%d ", freq[i]);
    }
    
    return 0;
}