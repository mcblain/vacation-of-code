#include <stdio.h>

int main() 
{
    char ch;
    char s[100]; 
    char sen[200];
    
    scanf("%c\n%s\n%[^\n]%*c\n", &ch, s, sen);
    
    printf("%c\n%s\n%s", ch, s, sen);

    return 0;
}