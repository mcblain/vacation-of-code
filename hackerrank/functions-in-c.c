#include <stdio.h>

int max_of_four(int a, int b, int c, int d) {
    int max_a = a < b ? b : a;
    int max_b = c < d ? d : c;
    return max_a < max_b ? max_b : max_a;  
}

int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);
    
    return 0;
}