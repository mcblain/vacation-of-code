{-# OPTIONS -Wall #-}

data Token
  = Null
  | Times
  | Div
  | Mod
  | And
  | Plus
  | Minus
  | Or
  | Eql
  | Neq
  | Lss
  | Leq
  | Gtr
  | Geq
  | Period
  | INT Int
  | FALSE
  | TRUE
  | Not
  | Lparen
  | Lbrak
  | Ident String
  | If
  | While
  | Repeat
  | Comma
  | Colon
  | Becomes
  | Rparen
  | Rbrak
  | Then
  | Of
  | Do
  | Semicolon
  | End
  | Else
  | Elsif
  | Until
  | Array
  | Record
  | Const
  | Type
  | Var
  | Procedure
  | Begin
  | Module
  | Eof
  deriving (Show)

type Offset = Int

type Line = Int

data Pos = Pos Line Offset deriving (Show)

data Lexer = Lexer String Pos deriving (Show)

isWhitespace :: Char -> Bool
isWhitespace c = c `elem` [' ', '\t', '\r']

isNewLine :: Char -> Bool
isNewLine c = c == '\n'

skipWhitespaces :: Lexer -> Lexer
skipWhitespaces lexer@(Lexer "" _) = lexer
skipWhitespaces lexer@(Lexer (c : cs) (Pos line pos))
  | isNewLine c = skipWhitespaces (Lexer cs (Pos (line + 1) 1))
  | isWhitespace c = skipWhitespaces (Lexer cs (Pos line (pos + 1)))
  | otherwise = lexer

skip :: Int -> Lexer -> Lexer
skip 0 lexer = lexer
skip _ lexer@(Lexer "" _) = lexer
skip n (Lexer (_ : cs) (Pos line pos)) =
  skip (n - 1) (Lexer cs (Pos line (pos + 1)))

isAtEnd :: String -> Bool
isAtEnd "" = Prelude.True
isAtEnd _ = Prelude.False

isAlpha :: Char -> Bool
isAlpha c = 'a' <= c && 'c' <= 'z' || 'A' <= c && c <= 'Z'

isDigit :: Char -> Bool
isDigit c = '0' <= c && c <= '9'

mark :: Lexer -> String -> IO ()
mark (Lexer _ (Pos line pos)) msg =
  putStrLn ("[" ++ show line ++ ":" ++ show pos ++ "]Error: " ++ msg)

eof :: Char
eof = '\0'

type Literal = String

number :: Lexer -> Literal -> (Token, Lexer)
number lexer@(Lexer src _) liter
  | src == "" = numToken
  | isDigit (head src) = number (skip 1 lexer) (head src : liter)
  | otherwise = numToken
  where
    numToken = (INT (read (reverse liter)), lexer)

matchKeyword :: Literal -> Token
matchKeyword liter = case liter of
  "DIV" -> Div
  "MOD" -> Mod
  "OR" -> Or
  "OF" -> Of
  "THEN" -> Then
  "DO" -> Do
  "UNTIL" -> Until
  "END" -> End
  "ELSE" -> Else
  "ELSIF" -> Elsif
  "IF" -> If
  "WHILE" -> While
  "REPEAT" -> Repeat
  "ARRAY" -> Array
  "RECORD" -> Record
  "CONST" -> Const
  "TYPE" -> Type
  "VAR" -> Var
  "PROCEDURE" -> Procedure
  "BEGIN" -> Begin
  "MODULE" -> Module
  _ -> Ident liter

identifier :: Lexer -> Literal -> (Token, Lexer)
identifier lexer@(Lexer src _) liter
  | src == "" = identToken
  | isAlpha (head src) = identifier (skip 1 lexer) (head src : liter)
  | otherwise = identToken
  where
    identToken = (matchKeyword (reverse liter), lexer)

get :: Lexer -> (Token, Lexer)
get lexer =
  let lexer'@(Lexer src _) = skipWhitespaces lexer
   in case src of
        "" -> (Eof, lexer')
        (c : cs) ->
          let next = if cs == "" then eof else head cs
              token t = (t, skip 1 lexer')
           in case c of
                '&' -> token End
                '*' -> token Times
                '+' -> token Plus
                '-' -> token Minus
                '=' -> token Eql
                '#' -> token Neq
                '<' -> if next == '=' then token Leq else token Lss
                '>' -> if next == '=' then token Geq else token Gtr
                ';' -> token Semicolon
                ',' -> token Comma
                ':' -> if next == '=' then token Becomes else token Colon
                '.' -> token Period
                '(' -> token Lparen
                ')' -> token Rparen
                '[' -> token Lbrak
                ']' -> token Rbrak
                '~' -> token Not
                _
                  | isDigit c -> number (skip 1 lexer') [c]
                  | isAlpha c -> identifier (skip 1 lexer') [c]
                  | otherwise -> token Null

-- where
--   err = mark lexer

initLexer :: String -> Lexer
initLexer src = Lexer src $ Pos 1 1

tokenizer :: Lexer -> [Token] -> [Token]
tokenizer _ tokens@(Eof:_) = reverse tokens
tokenizer lexer tokens =
  let (token, lex2) = get lexer
   in tokenizer lex2 (token : tokens)

tokenize :: String -> [Token]
tokenize src = tokenizer (initLexer src) []
