package main

import "core:fmt"
import "core:os"
import "chunk"
import "debug"
import "vm"

main :: proc() {
	args: []string = os.args
	fmt.println(args)	
	
	vm_inst: vm.VM;
	vm.init(&vm_inst)

	test_chunk: chunk.Chunk
	chunk.init(&test_chunk)
	
	constant := chunk.add_constant(&test_chunk, 1.2)

	using chunk.Op_Code

	chunk.write(&test_chunk, u8(Constant), 123)
	chunk.write(&test_chunk, constant, 123)


	constant = chunk.add_constant(&test_chunk, 3.4);
	chunk.write(&test_chunk, u8(Constant), 123);
	chunk.write(&test_chunk, constant, 123);

	chunk.write(&test_chunk, u8(Add), 123);

	constant = chunk.add_constant(&test_chunk, 5.6);
	chunk.write(&test_chunk, u8(Constant), 123);
	chunk.write(&test_chunk, constant, 123);

	chunk.write(&test_chunk, u8(Divide), 123);

	chunk.write(&test_chunk, u8(Negate), 123)
	chunk.write(&test_chunk, u8(Return), 123)	
	
	debug.disassemble_chunk(&test_chunk, "test chunk")
	
	vm.interpret(&vm_inst, &test_chunk)
	
	chunk.free(&test_chunk)
	vm.free(&vm_inst)	
}