package debug

import "../chunk"
import "../value"
import "core:fmt"

simple_instruction :: proc(name: string, offset: int) -> int {
	fmt.println(name)
	return offset + 1
}



constant_instruction :: proc(name: string, ch: ^chunk.Chunk, offset: int) -> int {
	constant: u8 = ch.code[offset + 1]
	fmt.printf("%-16s %4d '", name, constant)
	value.print_value(ch.constants[constant])
	fmt.println()

	return offset + 2
}

disassemble_instruction :: proc (ch: ^chunk.Chunk, offset: int) -> int {
	using chunk.Op_Code;
	
	if 0 < offset && ch.lines[offset] == ch.lines[offset-1] {
		fmt.print("   | ")
	} else {
		fmt.printf("%04d ", offset)
	}
	
	instruction: u8 = ch.code[offset]	
	
	switch instruction {
		case u8(Return): 
			return simple_instruction("Return", offset)
		case u8(Add):
			return simple_instruction("Add", offset)
		case u8(Subtract):
			return simple_instruction("Negate", offset)
		case u8(Multiply):
			return simple_instruction("Multiply", offset)
		case u8(Divide):
			return simple_instruction("Divide", offset)
		case u8(Negate):
			return simple_instruction("Negate", offset)
		case u8(Constant):
			return constant_instruction("Constant", ch, offset)
		case: {
			fmt.eprintln("Invalid OpCode")
			return offset + 1
		}
	}
	
	return offset + 1
}

disassemble_chunk :: proc(ch: ^chunk.Chunk, name: string) {
	fmt.println("==", name, "==")
	
	for offset: int = 0; offset < len(ch.code); {
		offset = disassemble_instruction(ch, offset)
	}
}