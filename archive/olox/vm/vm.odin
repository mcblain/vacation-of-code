package vm

import "core:fmt"
import "../chunk"
import "../value"

STACK_MAX :: 256

VM :: struct {
	chunk: ^chunk.Chunk,
	ip: uint,
	stack: [STACK_MAX]value.Value,
	stack_top: uint,
}

Interpret_Result :: enum {
	Ok,
	CompileError,
	RuntimeError
}

reset_stack :: proc(vm_inst: ^VM) {
	vm_inst.stack_top = 0
}

init :: proc(vm_inst: ^VM) {
	vm_inst.chunk = nil	
	vm_inst.stack_top = 0
	reset_stack(vm_inst)
}

free :: proc(vm_inst: ^VM) {}

interpret :: proc(vm_inst: ^VM, ch: ^chunk.Chunk) -> Interpret_Result {
	vm_inst.chunk = ch
	vm_inst.ip = 0
	
	return run(vm_inst)
}

read_byte :: proc(vm_inst: ^VM) -> u8 {
	vm_inst.ip += 1
	return vm_inst.chunk.code[vm_inst.ip - 1]
}

read_constant :: proc(vm_inst: ^VM) -> value.Value {
	return vm_inst.chunk.constants[read_byte(vm_inst)]
}

push :: proc(vm_inst: ^VM, val: value.Value) {
	vm_inst.stack[vm_inst.stack_top] = val
	vm_inst.stack_top += 1
}

pop :: proc(vm_inst: ^VM) -> value.Value {
	vm_inst.stack_top -= 1
 	return vm_inst.stack[vm_inst.stack_top]
}

get_binary_op_args :: proc(vm_inst: ^VM) -> (value.Value, value.Value) {
	b := pop(vm_inst)
	a := pop(vm_inst)
	return a, b
}
 
run :: proc(vm_inst: ^VM) -> Interpret_Result {
	using chunk.Op_Code
	
	when #defined(DEBUG_TRACE_EXECUTION) {
		fmt.print("          ")
		for i := 0; i < vm_inst.stack_top; i += 1 {\
			fmt.print("[ ")
			value.print_value(vm_inst.stack[i])
			fmt.print(" ]")
		}
		fmt.println()
		debug.disassemble_instruction(&vm_inst.chunk, vm_inst.ip)
	}

	for {
		instruction: u8 = read_byte(vm_inst)
		
		switch instruction {
			case u8(Constant): {
				constant := read_constant(vm_inst)	
				push(vm_inst, constant)
				value.print_value(constant)
				fmt.println()
			}
			case u8(Add): {
				a, b := get_binary_op_args(vm_inst)
				push(vm_inst, a + b)
			}
			case u8(Subtract): {
				a, b := get_binary_op_args(vm_inst)
				push(vm_inst, a - b)
			}
			case u8(Multiply): {
				a, b := get_binary_op_args(vm_inst)
				push(vm_inst, a * b)
			}
			case u8(Divide): {
				a, b := get_binary_op_args(vm_inst)
				push(vm_inst, a / b)
			}
			case u8(Negate): {
				push(vm_inst, -pop(vm_inst))
			}
			case u8(Return): {
				value.print_value(pop(vm_inst))
				fmt.println()
				return Interpret_Result.Ok
			}
		}
	}
}
