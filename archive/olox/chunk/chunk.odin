package chunk

import "../value"

Op_Code :: enum {
	Constant,
	Add,
	Subtract,
	Multiply,
	Divide,
	Negate,
	Return
}

Chunk :: struct {
		code: [dynamic]u8,
		constants: [dynamic]value.Value,
		lines: [dynamic]int,
}

add_constant :: proc(ch: ^Chunk, val: value.Value) -> u8 {
	append(&ch.constants, val)
	return u8(len(&ch.constants) - 1)
}

init :: proc(ch: ^Chunk) {
	ch.constants = make([dynamic]value.Value)
	ch.code = make([dynamic]u8)
	ch.lines = make([dynamic]int)
}

write :: proc(ch: ^Chunk, code: u8, line: int) {
	append(&ch.code, code)
	append(&ch.lines, line)
}

free :: proc(ch: ^Chunk) {
	delete(ch.code)
	delete(ch.constants)
	delete(ch.lines)
}