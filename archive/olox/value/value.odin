package value

import "core:fmt"

Value :: f32

print_value :: proc(val: Value) {
	fmt.print(val)
}