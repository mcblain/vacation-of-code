#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_WIDTH 400
#define DEFAULT_HEIGHT 200

typedef struct {
  bool snake;
  bool apple;
} Collision;

typedef struct {
  int x;
  int y;
} Pos;

typedef Pos Bone;
typedef Pos Apple;

typedef struct {
  Bone *bones;
  int len;
  int capacity;
} Snake;

typedef enum {
  UP,
  DOWN,
  LEFT,
  RIGHT,
  NONE,
} Direction;

typedef enum {
  INIT,
  PLAY,
  PAUSE,
  WIN,
  FAIL,
} State;

typedef struct {
  int w;
  int h;
  int speed;
  int score;
  Snake snake;
  State state;
  Apple apple;
  bool *_game_field;
} Context;

void init_snake(Snake *snake, int field_w, int field_h) {
  snake->bones = (Bone *)malloc(field_w * field_h * sizeof(Bone));
  snake->len = 0;
  snake->capacity = field_w * field_h;
}

void init_game_context(Context *context, int w, int h) {
  context->w = w;
  context->h = h;
  context->state = INIT;
  context->apple.x = 10;
  context->apple.y = 10;
  context->score = 0;
  context->_game_field = malloc(w * h * sizeof(bool));
  init_snake(&context->snake, w, h);
}

void move_apple(Context *context) {
  int i;
  int w = context->w;
  int h = context->h;
  bool *field = context->_game_field;
  Snake *snake = &context->snake;
  Bone *bones = snake->bones;
  Bone bone;
  int snake_len = snake->len;
  Apple apple = {-1, -1};

  memset(field, false, w * h);

  for (i = 0; i < snake_len; i += 1) {
    bone = bones[i];
    field[w * bone.y + bone.x] = true;
  }

  if (snake_len == w * h) {
    context->state = WIN;
  }

  apple.x = GetRandomValue(0, w - 1);
  apple.y = GetRandomValue(0, h - 1);

  for (;; apple.y += 1) {
    for (;; apple.x += 1) {
      if (!field[apple.y * w + apple.x]) {
        context->apple = apple;
        return;
      }

      if (apple.x == w) {
        apple.x = 0;
      }
    }

    if (apple.y == h) {
      apple.y = 0;
    }
  }
}

Bone get_new_head_pos(int old_x, int old_y, Direction direction) {
  Bone bone = {old_x, old_y};

  switch (direction) {
  case UP:
    bone.y -= 1;
    break;
  case DOWN:
    bone.y += 1;
    break;
  case LEFT:
    bone.x -= 1;
    break;
  case RIGHT:
    bone.x += 1;
  case NONE:
    break;
  }

  return bone;
}

Collision check_collision(Context *context, Direction direction) {
  int i;
  Snake *snake = &context->snake;
  Bone *bones = snake->bones;
  Bone head = bones[0];
  Bone bone;
  Apple apple = context->apple;
  int len = snake->len;
  Bone new_head = get_new_head_pos(head.x, head.y, direction);

  Collision collision = {false, false};

  if (new_head.x == apple.x && new_head.y == apple.y) {
    collision.apple = true;
  }

  if (3 < len) {
    for (i = 0; i < len; i += 1) {
      bone = bones[i];
      if (new_head.x == bone.x && new_head.y == bone.y) {
        collision.snake = true;
        break;
      }
    }
  }

  return collision;
}

void destroy_snake(Snake *snake) { free(snake->bones); }

void grow_snake(Snake *snake, int head_x, int head_y) {
  int i;
  int len = snake->len;
  Bone prev_bone;
  Bone temp_bone;

  if (len == snake->capacity) {
    return;
  }

  if (len == 0) {
    snake->bones[0].x = head_x;
    snake->bones[0].y = head_y;
    snake->len = 1;
    return;
  }

  prev_bone = snake->bones[0];

  snake->bones[0].x = head_x;
  snake->bones[0].y = head_y;

  for (i = 1; i <= len; i += 1) {
    temp_bone = snake->bones[i];
    snake->bones[i] = prev_bone;
    prev_bone = temp_bone;
  }

  snake->len += 1;
}

void grow_snake_direction(Snake *snake, Direction direction, int w, int h) {
  Bone new_head_pos =
      get_new_head_pos(snake->bones[0].x, snake->bones[0].y, direction);

  printf("Before: %d %d\n", new_head_pos.x, new_head_pos.y);

  new_head_pos.x =
      new_head_pos.x < 0 ? w - new_head_pos.x - 2 : new_head_pos.x % w;
  new_head_pos.y =
      new_head_pos.y < 0 ? h - new_head_pos.y - 2 : new_head_pos.y % h;

  printf("After: %d %d\n", new_head_pos.x, new_head_pos.y);

  grow_snake(snake, new_head_pos.x, new_head_pos.y);
}

void move_snake(Snake *snake, Direction direction, int w, int h) {
  grow_snake_direction(snake, direction, w, h);
  snake->len -= 1;
}

void destroy_game_context(Context *context) { destroy_snake(&context->snake); }

int main(int argc, char **argv) {
  int i;
  int x;
  int y;
  Bone bone;
  int color;
  int len;
  int cell_size;
  int scr_w;
  int scr_h;
  int offset_x;
  int offset_y;
  int frame_cnt = 0;
  Context context;
  Direction direction = NONE;
  int field_w = DEFAULT_WIDTH;
  int field_h = DEFAULT_HEIGHT;
  Collision collision;
  char str_buff[256];

  if (3 == argc) {
    field_w = atoi(argv[1]);
    if (field_w == 0)
      field_w = DEFAULT_WIDTH;
    field_h = atoi(argv[2]);
    if (field_h == 0)
      field_h = DEFAULT_HEIGHT;
  }

  init_game_context(&context, field_w, field_h);

  SetTraceLogLevel(LOG_NONE);
  SetConfigFlags(FLAG_WINDOW_RESIZABLE);
  InitWindow(field_w, field_h, "Snake");

  SetTargetFPS(60);

  grow_snake(&context.snake, 0, 0);

  while (!WindowShouldClose()) {

    frame_cnt += 1;

    if (IsKeyPressed(KEY_UP)) {
      if (direction != DOWN) {
        direction = UP;
      }
    }
    if (IsKeyPressed(KEY_DOWN)) {
      if (direction != UP) {
        direction = DOWN;
      }
    }
    if (IsKeyPressed(KEY_LEFT)) {
      if (direction != RIGHT) {
        direction = LEFT;
      }
    }
    if (IsKeyPressed(KEY_RIGHT)) {
      if (direction != LEFT) {
        direction = RIGHT;
      }
    }

    if (frame_cnt % 10 == 0) {
      collision = check_collision(&context, direction);

      if (collision.apple) {
        context.score += 1;
        context.speed += 1;
        grow_snake_direction(&context.snake, direction, context.w, context.h);
        move_apple(&context);
      } else {
        move_snake(&context.snake, direction, context.w, context.h);
      }

      if (collision.snake) {
        context.state = FAIL;
      } else {
        context.state = INIT;
      }
    }

    scr_w = GetScreenWidth();
    scr_h = GetScreenHeight();

    cell_size = scr_w < scr_h ? scr_w / context.w : scr_h / context.h;

    BeginDrawing();

    ClearBackground(CLITERAL(Color){10, 20, 0, 255});

    len = context.snake.len;

    offset_x = (scr_w % (cell_size * context.w)) / 2;
    offset_y = (scr_h % (cell_size * context.h)) / 2;

    DrawRectangle(offset_x, offset_y, scr_w - offset_x * 2,
                  scr_h - offset_y * 2, CLITERAL(Color){10, 30, 0, 255});

    for (i = len - 1; 0 <= i; i -= 1) {
      bone = context.snake.bones[i];

      x = bone.x * cell_size + offset_x;
      y = bone.y * cell_size + offset_y;

      color = (1.0f - (float)i / (float)len) * 150 + 105;

      DrawRectangle(x, y, cell_size, cell_size,
                    i == 0 ? WHITE : CLITERAL(Color){0, color, 0, 255});
    }

    if (0 <= context.apple.x) {
      x = context.apple.x * cell_size + offset_x;
      y = context.apple.y * cell_size + offset_y;

      DrawRectangle(x, y, cell_size, cell_size,
                    CLITERAL(Color){255, 0, 0, 255});
    }

    if (context.state == FAIL) {
      DrawText("FAIL", 0, 0, 24, WHITE);
    }

    sprintf(str_buff, "SCORE: %d", context.score);

    DrawText(str_buff, 0, 25, 24, WHITE);

    EndDrawing();
  }

  CloseWindow();
}
