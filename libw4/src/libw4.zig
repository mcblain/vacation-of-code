const w4 = @import("wasm4.zig");

var buttons_state: u8 = 0;
var buttons_difference: u8 = 0;

var mouse_state = MouseState.init(0, 0, 0);
var mouse_difference = MouseState.init(0, 0, 0);

pub const Color = struct {
    pub fn set(color_id: u4, color: u2) void {
        const x = w4.DRAW_COLORS.*;
        var mask = @intCast(u16, color) << 12;
        mask >>= @intCast(u4, (color_id * 4));
        w4.DRAW_COLORS.* ^= mask;
        w4.tracef("a: %d, b: %d", x, w4.DRAW_COLORS.*);
    }
};

const MouseState = struct {
    x: i16,
    y: i16,
    buttons: u8,

    fn init(x: i16, y: i16, buttons: u8) MouseState {
        return .{
            .x = x,
            .y = y,
            .buttons = buttons,
        };
    }
};

fn handleInput() void {
    const gamepad = w4.GAMEPAD1.*;
    buttons_difference = gamepad & (gamepad ^ buttons_state);
    buttons_state = gamepad;

    const m_x = w4.MOUSE_X.*;
    const m_y = w4.MOUSE_Y.*;
    const m_buttons = w4.MOUSE_BUTTONS.*;

    mouse_difference.x = m_x - mouse_state.x;
    mouse_difference.y = m_y - mouse_state.y;

    mouse_difference.buttons = m_buttons & (m_buttons ^ mouse_state.buttons);

    mouse_state.x = m_x;
    mouse_state.y = m_y;
    mouse_state.buttons = m_buttons;
}

pub const Mouse = struct {
    pub fn x() i16 {
        return mouse_state.x;
    }
    pub fn y() i16 {
        return mouse_state.y;
    }
    pub fn xDiff() i16 {
        return mouse_difference.x;
    }
    pub fn yDiff() i16 {
        return mouse_difference.y;
    }
    pub fn left() bool {
        return (mouse_state.buttons & w4.MOUSE_LEFT) != 0;
    }
    pub fn right() bool {
        return (mouse_state.buttons & w4.MOUSE_RIGHT) != 0;
    }
    pub fn middle() bool {
        return (mouse_state.buttons & w4.MOUSE_MIDDLE) != 0;
    }
};

pub const Btn = struct {
    const Button = enum(u8) {
        up = w4.BUTTON_UP,
        down = w4.BUTTON_DOWN,
        left = w4.BUTTON_LEFT,
        right = w4.BUTTON_RIGHT,
        a1 = w4.BUTTON_1,
        a2 = w4.BUTTON_2,
    };

    fn btnDown(btn: Button) bool {
        return (buttons_state & @enumToInt(btn)) != 0;
    }

    fn btnPressed(btn: Button) bool {
        return (buttons_difference & @enumToInt(btn)) != 0;
    }

    pub fn up() bool {
        return btnDown(Button.up);
    }

    pub fn down() bool {
        return btnDown(Button.down);
    }

    pub fn left() bool {
        return btnDown(Button.left);
    }

    pub fn right() bool {
        return btnDown(Button.right);
    }

    pub fn a1() bool {
        return btnDown(Button.a1);
    }

    pub fn a2() bool {
        return btnDown(Button.a2);
    }

    pub fn upPressed() bool {
        return btnPressed(Button.up);
    }

    pub fn downPressed() bool {
        return btnPressed(Button.down);
    }

    pub fn leftPressed() bool {
        return btnPressed(Button.left);
    }

    pub fn rightPressed() bool {
        return btnPressed(Button.right);
    }

    pub fn a1Pressed() bool {
        return btnPressed(Button.a1);
    }

    pub fn a2Pressed() bool {
        return btnPressed(Button.a2);
    }
};

///////////////////////////////////////////////////////////

// Update state
pub fn update() void {
    handleInput();
}

pub fn start() void {}
