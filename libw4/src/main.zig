const w4 = @import("wasm4.zig");
const libw4 = @import("libw4.zig");
const Btn = libw4.Btn;
const Mouse = libw4.Mouse;

const smiley = [8]u8{
    0b11000011,
    0b10000001,
    0b00100100,
    0b00100100,
    0b00000000,
    0b00100100,
    0b10011001,
    0b11000011,
};

var x: i32 = 76;
var y: i32 = 76;

export fn start() void {
    libw4.start();
}

var i: u2 = 0;

export fn update() void {
    // w4.tracef("x: %d, y: %d\n", x, y);
    libw4.update();

    w4.DRAW_COLORS.* = 2;
    w4.text("Hello from Zig!", 10, 10);

    x += Mouse.x();
    y += Mouse.y();

    libw4.Color.set(2, 3);
    libw4.Color.set(1, 2);

    // i += 1;

    w4.blit(&smiley, x, y, 8, 8, w4.BLIT_1BPP);
    w4.text("Press X to blink", 16, 90);
}
